

var arr = [];
var mobileNo = "";
var bankList;

$('#sdate').dateTimePicker({
	limitMin: moment(moment(), "YYYY-MM-DD").add(3, 'days'),
});

$('#edate').dateTimePicker({
	limitMin: moment(moment(), "YYYY-MM-DD").add(10, 'days'),
});

$(document).ready(function() {

    //$("#createform").reset();
	document.getElementById("createform").reset();
	 var input = document.getElementById("awesomeHu");

	 console.log(input)

	    var awesomplete = new Awesomplete(input, {
	        minChars: 1,
	        maxItems: 10, //how many items are needed to be displayed 
	        autoFirst: false
	    });

	$.ajax({
		url: "/bin/bankList", 
		type: "POST",
		data: null,
		success: function (res) {
			var response = JSON.parse(res);
			bankList = response;
			console.log(response);
			awesomplete.list = response.bankList
		},
		error: function (e) {
			console.log("Error in Bank List :: " + e);
		}
	});


	//----------Create E-NACH AJAX----------
	$.ajax({
		url: "/bin/createEnach", 
		type: "POST",
		data: null,
		success: function (res) {
			var response = JSON.parse(res);
			if(response.success == 'true') {
				console.log("Success in Create E-NACH");
			}
		},
		error: function (e) {
			console.log("Error in Create E-NACH Ajax :: " + e);
		}
	});
	//----------------------------------------

	var input = $('input[name=destinationBank]');

	//Character Validation
	function characterValidation(arg) {
		$(arg).on('paste',function (e) {
			e.preventDefault();
		});

		$(arg).keypress(function (e) {
			var regex = new RegExp(/^[a-zA-Z\s]+$/);
			var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
			if (regex.test(str)) {
				return true;
			}
			else {
				e.preventDefault();
				return false;
			}
		});
	}

	characterValidation('.customerName');
	characterValidation('.bankName');

	//Numeric Validation
	function numericValidation(arg) {
		$(arg).on('paste',function (e) {
			e.preventDefault();
		});

		$(arg).keydown(function (e) {
	        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
	            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
	            (e.keyCode >= 35 && e.keyCode <= 40)) {
	                 return;
	        }
	        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
	            e.preventDefault();
	        }
	    });
	}

	numericValidation('.accountNumber');
	numericValidation('.confirmAccountNo'); 
	numericValidation('.maximum_amount'); 
	numericValidation('.aadharId');

	var accountArrays=[];
	var resFlag="";
	if (document.cookie.length != 0) {
		var namevaluepair = document.cookie.split("; ");
		for (var i = 0; i < namevaluepair.length; i++) {
			var namevaluearray = namevaluepair[i].split("=");
			if (namevaluearray[0] == "flag") {
				resFlag = namevaluearray[1];
			}
		}
	}

	if(resFlag.toString()==="true"){
		var userMobNo = "";
		var dob = "";
		if (document.cookie.length != 0) {
			var namevaluepair = document.cookie.split("; ");
			for (var i = 0; i < namevaluepair.length; i++) {
				var namevaluearray = namevaluepair[i].split("=");
				if (namevaluearray[0] == "userMobNo") {
					userMobNo = namevaluearray[1];
				} if(namevaluearray[0] == "userDob") {
					dob = namevaluearray[1];
				}
			}
		}
		$.ajax({
			url: "/bin/dobVerify", 
			type: "POST",
			data: {
				dob: dob,
				mobile: userMobNo
			},
			success: function (res) {
				var response = JSON.parse(res);
				for(var i=0; i<response.length; i++) {
					accountArrays.push(response[i].Account_Number);
					var myvalue = "XXXX-XXXX-"+response[i].Account_Number.substring(8, 13);
					$('.accountNo').append('<option>'+myvalue+'</option>')
				}
			},
			error: function (e) {
				console.log("Error in Create E-NACH Ajax :: " + e);
			}
		});	
	} else {
		$.ajax({
			url: "/bin/createEnach", 
			type: "POST",
			data: null,
			success: function (res) {
				var response = JSON.parse(res);
				var cifDetails = JSON.parse(response.cifDetails);
				for(var i=0; i<cifDetails.length; i++) {
					accountArrays.push(cifDetails[i].Account_Number);
					var myvalue = "XXXX-XXXX-"+cifDetails[i].Account_Number.substring(8, 13);
					$('.accountNo').append('<option>'+myvalue+'</option>')
				}
			},
			error: function (e) {
				console.log("Error in Create E-NACH Ajax :: " + e);
			}
		});
	}


	$('.cmp-date-time-picker').click(function(){
		var maximum_amount = parseFloat($('input[name=maximum_amount]').val());
		var startDate = $('input[name=startDate]').val();
		var endDate = $('input[name=endDate]').val();		
		if(maximum_amount && startDate && endDate){
			calcTotalAmountAndInvestments();
		}
	});

	$('.transferFrequency, .maximum_amount').change(function(){
		var maximum_amount = parseFloat($('input[name=maximum_amount]').val());
		var startDate = $('input[name=startDate]').val();
		var endDate = $('input[name=endDate]').val();		
		if(maximum_amount && startDate && endDate){
			calcTotalAmountAndInvestments();
		}
	});

	$('#openRD').change(function() {
		var transferFrequency = $('.transferFrequency').val();
		var startDate = $('input[name=startDate]').val();
		var endDate = $('input[name=endDate]').val();
		var rdAmount = $('input[name=maximum_amount]').val();

		$('.rdTransferFrequency').html(transferFrequency);
		$('.rdAmount').html(rdAmount ? rdAmount : "NA");
		$('.rdStartDate').html(startDate ? startDate : "NA");
		$('.rdEndDate').html(endDate ? endDate : "NA");
		$(".recurringWrapper").toggle();
	});

	$('input[name=maximum_amount]').focusout(function(){
		var amount= $('input[name=maximum_amount]').val();
		if(amount!=""){
			if(amount<1000 || amount>100000){
				$('#alertBox').show();
				$('#alertMessage').html("Amount should be between 1000 to 100000");
			}
		}
	});


	$('input[name=aadharId]').focusout(function(){
		var aadhar= $('input[name=aadharId]').val();
		if(aadhar!=""){
			if(aadhar.length<12 || aadhar.length>16){
				$('#alertBox').show();
				$('#alertMessage').html("Aadhaar ID should be 12 to 16 characters");
			}
		}
	});

	$('.confirmbutton').click(function() {
		var logoURL = "http://10.60.57.12:4503/content/dam/iibPlatform/enach/images/logo.jpg";
		var options = {
				"callback": function(t) {
					if(t.hasOwnProperty('error_code')) {
						window.location.assign("/content/enach/welcome/login/fail.html?failureMessage="+t.message);
					} else { 
						window.location.assign("/content/enach/welcome/login/success.html?documentId="+t.digio_doc_id);
					}
				},
				"logo":  encodeURIComponent(logoURL),      
		};

		if (document.cookie.length != 0) {
			var namevaluepair = document.cookie.split("; ");
			for (var i = 0; i < namevaluepair.length; i++) {
				var namevaluearray = namevaluepair[i].split("=");
				if (namevaluearray[0] == "userMobNo") {
					mobileNo = JSON.parse(namevaluearray[1]);
				}
			}
		}

		var customerNameLabel = $('.customerNameLabel').html().split("<")[0];
		var bankNameLabel = $('.bankNameLabel').html().split("<")[0];
		var accountNumberLabel = $('.accountNumberLabel').html().split("<")[0];
		var confirmAccountLabel = $('.confirmAccountLabel').html().split("<")[0];
		var ifscLabel = $('.ifscLabel').html().split("<")[0];
		var maximumAmountLabel = $('.maximumAmountLabel').html().split("<")[0];
		var transferFreqLabel = $('.transferFreqLabel').html().split("<")[0];
		var startDateLabel = $('.startDateLabel').html().split("<")[0];
		var endDateLabel = $('.endDateLabel').html().split("<")[0];
		var totalAmountLabel = $('.totalAmountLabel').html().split("<")[0];
		var totalInstallmentsLabel = $('.totalInstallmentsLabel').html().split("<")[0];
		var aadharLabel = $('.aadharLabel').html().split("<")[0];
		var referralCodeLabel = $('.referralCodeLabel').html().split("<")[0];
		var creditAccountLabel = $('.creditAccountLabel').html().split("<")[0];

		var customerName = $('input[name=customerName]').val() ? $('input[name=customerName]').val() : "";
		var destinationBank = $('input[name=destinationBank]').val();
		var destinationBankId = $('input[name=destinationBankId]').val();
		var customerAccountNo = $('input[name=accountNumber]').val();
		var confirmAccountNo = $('.confirmAccountNo').val();
		var maximum_amount = $('input[name=maximum_amount]').val();
		var transferFrequency = $('.transferFrequency').val();
		var startDate = $('input[name=startDate]').val();
		var endDate = $('input[name=endDate]').val();
		var totalAmount = $('.totalAmount').val();
		var totalInstallments = $('.totalInstallments').val();
		var aadharId = $('input[name=aadharId]').val().length;
		var aadharNo = $('input[name=aadharId]').val();
		var accLastDigits = $('.accountNo').val().substr(10,14);
		var referralCode = $('input[name=referralCode]').val();
		var accountNo="";
		for(let i=0; i<accountArrays.length; i++) {
			let n = accountArrays[i].includes(accLastDigits);
			if(n==true) {
				accountNo = accountArrays[i];
				break;
			}
		}

		let formData = {
				destinationBank : destinationBank,
				destinationBankId : destinationBankId,
				customerAccountNo : customerAccountNo,
				maximum_amount : maximum_amount,
				mobileNo : mobileNo,
				customerName : customerName, // To pick from API/Cookie
				transferFrequency : transferFrequency,
				startDate : startDate,
				endDate : endDate,
				accountNo : accountNo,
				confirmAccountNo : confirmAccountNo,
				totalAmount : totalAmount,
				totalInstallments : totalInstallments,
				referralCode : referralCode,
				aadharNo : aadharNo,
				customerNameLabel : customerNameLabel,
				bankNameLabel : bankNameLabel,
				accountNumberLabel : accountNumberLabel,
				confirmAccountLabel : confirmAccountLabel,
				ifscLabel : ifscLabel,
				maximumAmountLabel : maximumAmountLabel,
				transferFreqLabel : transferFreqLabel,
				startDateLabel : startDateLabel,
				endDateLabel : endDateLabel,
				totalAmountLabel : totalAmountLabel,
				totalInstallmentsLabel : totalInstallmentsLabel,
				aadharLabel : aadharLabel,
				referralCodeLabel : referralCodeLabel,
				creditAccountLabel : creditAccountLabel
		}

		var accountNumber = $('input[name=accountNumber]').val();
		var confirmAccountNumber = $('input[name=customerAccountNo]').val();
		var confirmAccountNumberLength = $('input[name=customerAccountNo]').val().length;
		var accountNumberLength = $('input[name=accountNumber]').val().length;

		if(customerName == ""){
			$('#alertBox').show();
			$('#alertMessage').html("Please enter a customer name");
		}else if(destinationBank == ""){
			$('#alertBox').show();
			$('#alertMessage').html("Bank name should not be blank");
		}else if(accountNumberLength < 12 || accountNumberLength > 34){
			$('#alertBox').show();
			$('#alertMessage').html("Please enter a valid account number");
		}else if(accountNumber != confirmAccountNumber){
			$('#alertBox').show();
			$('#alertMessage').html("Account number missmatch, failed to confirm");
		}else if(maximum_amount == ""){
			$('#alertBox').show();
			$('#alertMessage').html("Amount should not be blank");
		}else if(startDate == ""){
			$('#alertBox').show();
			$('#alertMessage').html("Start-Date should not be blank");
		}else if(endDate == ""){
			$('#alertBox').show();
			$('#alertMessage').html("End-Date should not be blank");
		}else if(accountNumber != confirmAccountNumber){
			$('#alertBox').show();
			$('#alertMessage').html("Account numbers did not match, Please re-enter the account numbers.");
		}else if(!destinationBankId.match(/[A-Z|a-z]{4}[a-zA-Z0-9]{7}$/)){
			$('#alertBox').show();
			$('#alertMessage').html("Please enter a valid IFSC number");
		}else if(transferFrequency == "Monthly" && calculateMonthsInBetween(startDate,endDate) < 1) {
			$('#alertBox').show();
			$('#alertMessage').html("There Should be atleast one installment in between Start-Date and End-Date.");
		}else if(transferFrequency == "Weekly" && calculateWeeksInBetween(startDate,endDate) < 1) {
			$('#alertBox').show();
			$('#alertMessage').html("There Should be atleast one installment in between Start-Date and End-Date.");
		}else if(parseInt(maximum_amount) > 100000) {
			$('#alertBox').show();
			$('#alertMessage').html("Please enter a transfer amount less than 1,00,000");
		}else if(aadharId < 12 || aadharId > 16){
			$('#alertBox').show();
			$('#alertMessage').html("Please enter a valid Aadhar/Virtual ID number");
		}else{   		
			var digio = new Digio(options);
			digio.init();
			$.ajax({
				url: "/bin/digioCreateMandate", 
				type: "POST",
				data: formData,
				success: function (res) {
					var response = JSON.parse(res);
					console.log(res);
					if(response.id) {
						digio.esign(response.id,mobileNo);
					} else {
						digio.cancel();
					}
				},
				error: function (e) {
					console.log("Error in Digio Create Mandate Ajax :: " + e);
				}
			});
		}		
	});

	$(".alertBtn").click(function () {
		$("#alertBox").hide();
	});

	function calcTotalAmountAndInvestments() {
		var maximum_amount = parseFloat($('input[name=maximum_amount]').val());
		var transferFrequency = $('.transferFrequency').val();
		var startDate = $('input[name=startDate]').val();
		var endDate = $('input[name=endDate]').val();		
		if(transferFrequency == "Monthly"){
			var noOfMonths = calculateMonthsInBetween(startDate,endDate);
			$('#totalAmount').val(maximum_amount * noOfMonths);
			$('#totalInstallments').val(noOfMonths);
		}else if(transferFrequency == "Weekly"){
			var noOfWeeks = calculateWeeksInBetween(startDate,endDate);
			$('#totalAmount').val(maximum_amount * noOfWeeks);
			$('#totalInstallments').val(noOfWeeks);
		}else if(transferFrequency == "Yearly"){
			var noOfYears = calculateYearsInBetween(startDate,endDate);
			$('#totalAmount').val(maximum_amount * noOfYears);
			$('#totalInstallments').val(noOfYears);
		}else if(transferFrequency == "Adhoc"){
			var noOfAdhoc = 1;
			$('#totalAmount').val(maximum_amount * noOfAdhoc);
			$('#totalInstallments').val(noOfAdhoc);
		}
	}

	function calculateMonthsInBetween(startDate,endDate) {
		from = moment(startDate, 'YYYY-MM-DD'); 
		to = moment(endDate, 'YYYY-MM-DD');     
		duration = to.diff(from, 'months');
		return duration;
	}

	function calculateWeeksInBetween(startDate,endDate) {
		from = moment(startDate, 'YYYY-MM-DD'); 
		to = moment(endDate, 'YYYY-MM-DD');     
		duration = to.diff(from, 'weeks');
		return duration;
	}

    function calculateYearsInBetween(startDate,endDate) {
		from = moment(startDate, 'YYYY-MM-DD'); 
		to = moment(endDate, 'YYYY-MM-DD');     
		duration = to.diff(from, 'years');
		return duration;
	}
});

