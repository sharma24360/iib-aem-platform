Handlebars.registerHelper('json', function(context) {
	return JSON.stringify(context);
});

function getMandateDetails(handle) {
	if(handle.umrn){
		window.location.assign('/content/enach/welcome/login/details.html?umrn='+ handle.umrn);	
	}
}

function cancelMandate(handle) {
	var mobileNo = "";
	if (document.cookie.length != 0) {
		var namevaluepair = document.cookie.split("; ");
		for (var i = 0; i < namevaluepair.length; i++) {
			var namevaluearray = namevaluepair[i].split("=");
			if (namevaluearray[0] == "userMobNo") {
				mobileNo = JSON.parse(namevaluearray[1]);
			}
		}
	};

	let formData = {
			destinationBank : handle.bankName,
			destinationBankId : 'INDB0000006',
			mobileNo : mobileNo,
			mandate_id : handle.umrn,
	};
	
	console.log(formData);

	$('#alertMessage').html('Proceed to CANCEL mandate ?');
	$('#alertBox').show();
	$('.alertBtn').on('click', function(e) {
		console.log('clicked');
		$('#alertBox').hide();
		$.ajax({
			url: "/bin/digioCancelMandate", 
			type: "POST",
			data: formData,
			success: function (res) {
				var response = JSON.parse(res);
				console.log(res);
			},
			error: function (e) {
				console.log("Error in Digio Cancel Mandate Ajax :: " + e);
			}
		});
	});
};

$(document).ready(function() {

	//----------------Store Mandate Data AJAX-------

	$.ajax({
		url: "/bin/manageMandateData", 
		type: "POST",
		data: null,
		success: function (res) {
			var response = JSON.parse(res);
			if(response.success == 'true') {
				console.log("Success in Manage Accounts");
			}
		},
		error: function (e) {
			console.log("Error in Manage Accounts :: " + e);
		}
	});
	//--------------------------------------------

	var rawTemplate = $('#mandateRows').html();

	$.ajax({
		url: "/bin/manageMandateData", 
		type: "POST",
		data: null,
		success: function (res) {
			var response = JSON.parse(res);
			var mandateData = JSON.parse(response.mandate);
			
			if(response.success = 'true') {
				extractResponse(mandateData);
			}
		},
		error: function (e) {
			console.log("Error in Extracting Response :: "+ e);
		}
	});


	function extractResponse(mandateData) {
		if(mandateData.length > 0){
			$('.errorMsg').hide();
			var compiledTemplate = Handlebars.compile(rawTemplate);
			var ourGeneratedHTML = compiledTemplate(mandateData);
			var dataContainer = document.getElementById("mandateBody");
			dataContainer.innerHTML = ourGeneratedHTML;
		}		
	}
});