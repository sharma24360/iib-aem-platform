var user = '';
var userUnParsed;

function getBrowserId() {

    var
        aKeys = ["MSIE", "Firefox", "Safari", "Chrome", "Opera"],
        sUsrAg = navigator.userAgent,
        nIdx = aKeys.length - 1;
    for (nIdx; nIdx > -1 && sUsrAg.indexOf(aKeys[nIdx]) === -1; nIdx--);
    return nIdx;
}


$.urlParam = function(name) {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results == null) {
        return null;
    } else {
        return decodeURI(results[1]) || 0;
    }
}

if (document.cookie.length != 0) {

    var namevaluepair = document.cookie.split("; ");
    for (var i = 0; i < namevaluepair.length; i++) {

        var namevaluearray = namevaluepair[i].split("=");
        if (namevaluearray[0] == "userCookie") {
            user = JSON.parse(namevaluearray[1]);
            userUnParsed = JSON.parse(user)

        }
    }
}


var num = getBrowserId();
var txnAmt = '';
var txnRefNo = '';
var bankRefNo = '';

if (num != -1) {
    urlParams = new URLSearchParams(window.location.search);
    txnAmt = urlParams.get('txnAmt');
    txnRefNo = urlParams.get('txnRefNo');
    bankRefNo = urlParams.get('bankRefNo');

} else if (num == -1) {
    txnAmt = $.urlParam('txnAmt');
    txnRefNo = $.urlParam('txnRefNo')
    bankRefNo = $.urlParam('bankRefNo')
}

if (!txnAmt) {
window.location.assign('/content/enach-pl/home/registration-pl.html');

} else {
    if (user != '') {
        $('.userName').html(userUnParsed.Name);
    }

    $('.refNo').html(txnRefNo);
    $('.amt').html(txnAmt);
    $('.bankrefNo').html(bankRefNo);




}