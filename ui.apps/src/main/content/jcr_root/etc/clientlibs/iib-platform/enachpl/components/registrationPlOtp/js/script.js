var count = 0;
var user = '';
var userUnParsed;
var mobileNumber = '';

if (document.cookie.length != 0) {

	var namevaluepair = document.cookie.split("; ");
	for (var i = 0; i < namevaluepair.length; i++) {

		var namevaluearray = namevaluepair[i].split("=");
		if (namevaluearray[0] == "userCookie") {
			user = JSON.parse(namevaluearray[1]);
			userUnParsed = JSON.parse(user)
			mobileNumber = userUnParsed.MobileNo
			//console.log(mobileNumber);
		}
	}
}

$(document).ready(function() {
	$(".otpBox").keydown(function (e) {
	    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
	        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
	        (e.keyCode >= 35 && e.keyCode <= 40)) {
	             return;
	    }
	    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
	        e.preventDefault();
	    }
	});
});

$(document).on('click', '.confirmbutton', function(event) {
	if (count < 3) {
		count = count + 1;
		console.log(count)
		const otp = $('.enachofferInput').val();
		if (!otp) {

			$('.eNACHAlert').html("Please Enter the Otp");
			$('#enachAlert').show();
			$('.hvr-btn').on('click', function(e) {
				$('#enachAlert').hide();
			});

		} else {

			$.ajax({
				url: '/bin/verifyotp',
				type: 'GET',
				data: {
					otp: otp,
					mobile: mobileNumber
				},

				success: function(msg) {

					console.log("Otp response :", msg);
					if (msg == "Verified") {
						window.location.assign('/content/enach-pl/home/personal-loan-details.html');
					} else {
						$('.eNACHAlert').html("Invalid OTP");
						$('#enachAlert').show();
						$('.hvr-btn').on('click', function(e) {
							$('#enachAlert').hide();
						});
					}

				},
				error: function(e) {
					console.log(e);
				}

			})
		}
	} else {
		$('.eNACHAlert').html("You have exceeded the Otp Limit, You will be redirected to Home Page");
		$('#enachAlert').show();
		$('.hvr-btn').on('click', function(e) {
			window.location.assign('/content/enach-pl/home/registration-pl.html')
		});

	}
});
