var tid = "";
var status = "";
var mobile = '';
var mobileRegex = /(7|8|9)\d{9}$/;
var urldata;
var landingUrl = localStorage.getItem('url');


$(document).ready(function(){
    $(".mobile").val("");
    $(".Otp").val("");
    $(".dobVerify").val("");
	var searchParams = new URLSearchParams(window.location.search);
	var param = searchParams.get('mode');
	urldata = param;
});

//Numeric Validation
function numericValidation(arg) {
	$(arg).on('paste',function (e) {
		e.preventDefault();
	});

	$(arg).on('input',function (e) {
		var mobile = $(arg).val();

		if(mobile.length==1 && mobile!=9 && mobile!=8 && mobile!=7){
			var mobilestr = mobile.substring(0, mobile.length-1);
			$(arg).val( mobilestr);
		}

		if(isNaN(mobile) || mobile.includes(".")){
			var mobilestr = mobile.substring(0, mobile.length-1);
			$(arg).val( mobilestr);

		}
	});
}

function otpValidation(arg) {
	$(arg).on('paste',function (e) {
		e.preventDefault();
	});

	$(arg).on('input',function (e) {
		var otp = $(arg).val();
		var otpRe = /\d/g;

		if(isNaN(otp) || otp.includes(".") || otp.length>6){
			var otpstr = otp.substring(0, otp.length-1);
			$(arg).val(otpstr);

		}
	});
}
//---------------------------------------

numericValidation('.mobile');
otpValidation('.otpverify');

$(document).on('click', '.otpSms', function () {
	mobile = $('.mobile').val();
	var otpverify = $('.otpverify').val();
	if (tid != "") {
		$.ajax({
			url: "/bin/enachVerify?mobile=" + mobile
			+ "&otp=" + otpverify,
			type: "GET",
			success: function (result) {
				if (result == "Verified") {
					$(".overlay").show();
					$.ajax({
						url: "/bin/accountDetails", 
						type: "POST",
						data: {
							mobile: mobile
						},
						success: function (res) {
							console.log(res);
							var resObj = JSON.parse(res);
							$(".overlay").hide();
							if(resObj.success == 'true' && resObj.isDobRequired=='false') {
								document.cookie = "flag=false; path=/";
								window.location.assign(landingUrl);
							} else if(resObj.success == 'false' && resObj.isDobRequired=='true') {
								$('.loginForm').hide(); 
								$('.dobForm').removeAttr('hidden');
							} else {
								$('#alertBox').show();
								$('#alertMessage').html('Error Fetching details');
							}
						},
						error: function (e) {
							console.log("Error in Verify OTP:: " + e);
						}
					});
				} else {
					$('#alertBox').show();
					$('#alertMessage').html(result);
				}
			},
			error: function (e) {
				console.log("Error in Verify OTP:: " + e);
			}
		});
	}

	else if (mobile == "") {
		$('#alertBox').show();
		$('#alertMessage').html("Please Enter Your Mobile Number");
	} else if(!mobileRegex.test(mobile)) {
		$('#alertBox').show();
		$('#alertMessage').html("Please Enter Correct Mobile Number!");
	} else {
		$.ajax({
			url: "/bin/generateOtp?mobile=" + mobile,
			type: "GET",
			success: function (result) {
				var real = JSON.parse(result);
				tid = real.TID;
				$('.Otp').removeAttr('hidden');
				//Start the timer
				$('.resendTimerBox').show();
				document.getElementById('resendTimer').innerHTML = 01 + ":" + 00;
				startTimer();

			},
			error: function (e) {
				console.log("Error in Generate OTP:: " + e);
			}
		});
	}
	return false;
});

$(document).on('click', '#sendOtpCall', function () {
	//var mobile = $('.mobile').val();
	if (mobile != "") {
		$.ajax({
			url: "/bin/soapCallOTP?mobile=" + mobile,
			type: "GET",
			success: function (result) {
				var res = JSON.parse(result);
				status = res.status;
				console.log("Status: " + res.status);
				if (status == "success") {
					$('#alertBox').show();
					$('#alertMessage').html('OTP has been sent!');
					//Start the timer
					$('.resendTimerBox').show();
					document.getElementById('resendTimer').innerHTML = 01 + ":" + 00;
					startTimer();
				}
			},
			error: function (e) {
				console.log("Error in SOAP Request OTP:: " + e);
			}
		});
	}
});

$(document).on('click', '#sendOtpText',function () {
	if ($('.mobile').val() == "") {
		$('#alertBox').show();
		$('#alertMessage').html("Please fill value");
	} else {
		$.ajax({
			url: "/bin/generateOtp?mobile=" + mobile,
			type: "GET",
			success: function (result) {
				$('#alertBox').show();
				$('#alertMessage').html("OTP has been successfully sent");
				//Start the timer
				$('.resendTimerBox').show();
				document.getElementById('resendTimer').innerHTML = 01 + ":" + 00;
				startTimer();
			},
			error: function (e) {
				console.log("Error in Send OTP:: " + e);
			}
		});
	}
});

//DOB Verify AJAX
$(document).on('click', '.dobBtn', function () {
	var day, month, year;
	var date = new Date($('#date-input').val());
	day = ((date.getDate())>=10)? (date.getDate()) : '0' + (date.getDate());
	month = ((date.getMonth()+1)>=10)? (date.getMonth()+1) : '0' + (date.getMonth()+1);
	year = date.getFullYear();
	var dob = [day, month, year].join('-');

	if (dob != "") {
		$.ajax({
			url: "/bin/dobVerify",
			type: "POST",
			data: {
				dob: dob,
				mobile: mobile
			},
			success: function (result) {
				console.log(result);
				if(JSON.parse(result).length == 0){
					$('#alertBox').show();
					$('#alertMessage').html('DOB doesn\'t exists!');
				} else {
					document.cookie = "flag=true; path=/";
					window.location.assign(landingUrl);
					
				}

			},
			error: function (e) {
				console.log("Error in E-NACH DOB:: " + e);
			}
		});
	}
});

$(".alertBtn").click(function () {
	$("#alertBox").hide();
});

function startTimer() {
	$('.resendViaSms').hide();
	$('.resendViaCall').hide();
	var presentTime = document.getElementById('resendTimer').innerHTML;
	var timeArray = presentTime.split(/[:]+/);
	var m = timeArray[0];
	var s = checkSecond((timeArray[1] - 1));
	if (s == 59) {
		m = m - 1;
	};
	if (m < 0) {
		$('.resendTimerBox').hide();
		$('.resendViaSms').show();
		$('.resendViaCall').show();
	} else {
		document.getElementById('resendTimer').innerHTML = m + ":" + s;
		setTimeout(startTimer, 1000);
	}
}

function checkSecond(sec) {
	if (sec < 10 && sec >= 0) {
		sec = "0" + sec;
	}; // add zero in front of numbers < 10
	if (sec < 0) {
		sec = "59";
	};
	return sec;
}