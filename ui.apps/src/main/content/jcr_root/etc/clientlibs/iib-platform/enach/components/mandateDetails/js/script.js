var umrnNumber = "";
$(document).ready(function() {	
	var num = getBrowserId();
	function getBrowserId() {
	    var
	        aKeys = ["MSIE", "Firefox", "Safari", "Chrome", "Opera"],
	        sUsrAg = navigator.userAgent,
	        nIdx = aKeys.length - 1;
	    for (nIdx; nIdx > -1 && sUsrAg.indexOf(aKeys[nIdx]) === -1; nIdx--);
	    return nIdx;
	}

	$.urlParam = function(name) {
	    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
	    if (results == null) {
	        return null;
	    } else {
	        return decodeURI(results[1]) || 0;
	    }
	}

	if (num != -1) {
	    urlParams = new URLSearchParams(window.location.search);
	    umrnNumber = urlParams.get('umrn');
	} else if (num == -1) {
		umrnNumber = $.urlParam('umrn');
	}

	$.ajax({
		url: "/bin/getMandateDetails", 
		type: "POST",
		data: {umrnNumber : umrnNumber},
		success: function (res) {
			var resObj = JSON.parse(res);
			var emandateMaster = resObj.response.emandateMaster;
			console.log(emandateMaster);
			if(resObj.header.successMsg == "SUCCESS"){
				$('.bankName').html(emandateMaster.corporateName);
				$('.AcNo').html(emandateMaster.sponserBankCode);
				$('.status').html(emandateMaster.mandateStatus);
				$('.amount').html(emandateMaster.amount);
				$('.umrn').html(emandateMaster.umrn);
				$('.startdate').html(emandateMaster.mndStartDate.substring(0, 10));
				$('.endDate').html(emandateMaster.mndEnddate.substring(0, 10));
			}else {
				console.log(resObj);
				//window.location.assign('/content/enach/login/manage.html');
			}
		},
		error: function (e) {
			console.log("Error in Extracting Response :: "+ e);
		}
	});
});