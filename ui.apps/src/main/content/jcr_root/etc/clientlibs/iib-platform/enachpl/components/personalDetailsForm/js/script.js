var repaymentBankCode='';
var flag = false;
var bankName;

$(document).ready(function() {	
	
	$.ajax({
   		type : "POST",
   		url : "/bin/repaymentBankVerify",
   		data : {
   			repaymentBankCode : repaymentBankCode
   		},
   		success : function(data) {
   			var result = JSON.parse(data);
   			bankName = result.bankName;
   			
   			if(result.success=='true') {
   				$('#repaymentBankName').html(bankName);
   				
   				$("#proceedBtn").attr('disabled','disabled');
   				$('#freqDropdown').val(result.frequencyType);
   				$('#freqDropdown').attr("disabled", true);
   			} else {
   				$('#repaymentBankName').html('NA');
   			}
   		},
   		error : function(error) {
   			console.log("Something really bad happened " + error);
   		}
   	});
	

	/*Captilization of each Character
	$("input").bind('keydown', function (event) {
	    if (event.keyCode > 64 && event.keyCode < 91) {
	        $(this).val(function (i, val) {
	            return val + String.fromCharCode(event.keyCode).toUpperCase();
	        });
	        return false;
	    }
	});*/
	
});

$('input[name=bankIFSCcode]').change(function() {

$('#ifscMsg').hide();
$('#c').prop('checked',false);
});

$('input[name=confirmIFSC]').change(function() {
	var ifsc = $('#bankIFSCcode').val();
	var confirmIfsc = $('#confirmBankIFSCcode').val();
	//var ifscRegex = /^[A-Za-z]{4}\d{7}$/;
	var ifscRegex = /^[A-Za-z]{4}0[A-Z0-9a-z]{6}$/
	
	if( (ifsc == confirmIfsc) && (ifscRegex.test(ifsc)) && (ifscRegex.test(confirmIfsc)) ) {
		$('#ifscMsg').hide();
		//console.log('IFSC Confirmed');
		$.ajax({
	   		type : "POST",
	   		url : "/bin/ifscVerify",
	   		data : {
	   			ifsc : confirmIfsc,
	   			repaymentBankName : bankName
	   		},
	   		success : function(data) {
	   			var res = JSON.parse(data);
	   			console.log(res);
	   			if(res.success == 'true') {
	   				flag = true;
	   				$("#proceedBtn").removeAttr('disabled');

	   			} else {
	   				$('#ifscMsg').show();
	   				$('#ifscMsg').html('Please enter a valid IFSC code');
	   			}
	   		},
	   		error : function(error) {
	   			console.log("Something really bad happened " + error);
	   		}
	   	});
	} else {
		$("#proceedBtn").attr('disabled','disabled');
		$('#ifscMsg').show();
		$('#ifscMsg').html('Please enter a valid IFSC code');
	}
});

$(document).on('click', '#proceedBtn', function (event) {
	var bankCode = $('#bankIFSCcode').val()
	var confirmBankCode = $('#confirmBankIFSCcode').val()
	var iAgree = $('#c')[0].checked;
	var iAgreeConfirmed = $('#d')[0].checked;
	var clickedFreq = $('#freqDropdown').val();
	var bankAccountType = $('#bankAccountType').val();

	if (!bankCode) {
		$('.eNACHAlert').html("Please Enter the IFSC Code");
		$('#enachAlert').show();
		$('.hvr-btn').on('click', function(e) {
			$('#enachAlert').hide();
		});

	} else if (!confirmBankCode) {
		$('.eNACHAlert').html("Please Enter the IFSC Code");
		$('#enachAlert').show();
		$('.hvr-btn').on('click', function(e) {
			$('#enachAlert').hide();
		});
	} else if (confirmBankCode != bankCode) {
		$('.eNACHAlert').html("The IFSC Code doesn't match");
		$('#enachAlert').show();
		$('.hvr-btn').on('click', function(e) {
			$('#enachAlert').hide();
		});
	} else if(flag==false) {
		$('.eNACHAlert').html("Please enter a valid IFSC code");
		$('#enachAlert').show();
		$('.hvr-btn').on('click', function(e) {
			$('#enachAlert').hide();
		});
	}
	else if (iAgree == false ||  iAgreeConfirmed == false)
	{
		$('.eNACHAlert').html("Please agree to confirm to all Conditions");
		$('#enachAlert').show();
		$('.hvr-btn').on('click', function(e) {
			$('#enachAlert').hide();
		});
	}
	else {

		$('.eNACHAlert').html("You will now be leaving the IndusInd Bank Ltd. Website and be taken to the BillDesk portal.");
		$('#enachAlert').show();
		$('.hvr-btn').on('click', function(e){
			$.ajax({
				type : "POST",
				data : {
					ifsc: confirmBankCode,
					frequency: clickedFreq,
					bankAccountType: bankAccountType
				},
				url : "/bin/billDeskService",
				success : function(data) {
					window.location.assign(data);
				},
				error : function(error) {
					console.log(error);
				}
			});
		});
	}
});

function addCommas(nStr) {
	nStr += '';
	var x = nStr.split('.');
	var x1 = x[0];
	var x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}

var a = ['','One ','Two ','Three ','Four ', 'Five ','Six ','Seven ','Eight ','Nine ','Ten ','Eleven ','Twelve ','Thirteen ','Fourteen ','Fifteen ','Sixteen ','Seventeen ','Eighteen ','Nineteen '];
var b = ['', '', 'Twenty','Thirty','Forty','Fifty', 'Sixty','Seventy','Eighty','Ninety'];
function inWords (num) {
    if ((num = num.toString()).length > 9) return 'overflow';
    n = ('000000000' + num).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
    if (!n) return; var str = '';
    str += (n[1] != 0) ? (a[Number(n[1])] || b[n[1][0]] + ' ' + a[n[1][1]]) + 'Crore ' : '';
    str += (n[2] != 0) ? (a[Number(n[2])] || b[n[2][0]] + ' ' + a[n[2][1]]) + 'Lakh ' : '';
    str += (n[3] != 0) ? (a[Number(n[3])] || b[n[3][0]] + ' ' + a[n[3][1]]) + 'Thousand ' : '';
    str += (n[4] != 0) ? (a[Number(n[4])] || b[n[4][0]] + ' ' + a[n[4][1]]) + 'Hundred ' : '';
    str += (n[5] != 0) ? ((str != '') ? 'and ' : '') + (a[Number(n[5])] || b[n[5][0]] + ' ' + a[n[5][1]]) + 'Rupees ' : '';
    return str;
}

var user = "";
var userUnParsed = {};
if (document.cookie.length != 0) {

	var namevaluepair = document.cookie.split("; ");
	for (var i = 0; i < namevaluepair.length; i++) {

		var namevaluearray = namevaluepair[i].split("=");
		if (namevaluearray[0] == "userCookie") {
			user = JSON.parse(namevaluearray[1]);
			userUnParsed = JSON.parse(user)
			repaymentBankCode = userUnParsed.Bank_code;
		}
	}
}

var emiString = inWords(userUnParsed.EMI_amount);
var instAmountString = inWords(userUnParsed.Instructed_amt);

if(user != '') {

	$('#userName').html(userUnParsed.Name);
	$('#aadharCard').html(userUnParsed.AadharNo);

	$('#loanNumber').html(userUnParsed.Name);
	$('#startDate').html(userUnParsed.AadharNo);

	$('#loanNumber').html(userUnParsed.loanNumber);
	$('#startDate').html(userUnParsed.OriginalDate);

	$('#EMI_amount').html(addCommas(userUnParsed.EMI_amount) + " | " + emiString);
	$('#SI_amount').html(addCommas(userUnParsed.Instructed_amt + " | " + instAmountString));

	$('#Other_bank_account_number').html(userUnParsed.Other_bank_account_number);

	$('#bank').html(userUnParsed.Bank_short_name);

}else{
	window.location.assign('/content/enach-pl/home/registration-pl.html');
}
