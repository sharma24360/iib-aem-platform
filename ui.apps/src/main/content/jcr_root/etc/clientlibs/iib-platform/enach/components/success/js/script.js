var num = getBrowserId();
var documentId = '';
var interactionId = '';

$(document).ready(function(){

	if (document.cookie.length != 0) {
		var namevaluepair = document.cookie.split("; ");
		for (var i = 0; i < namevaluepair.length; i++) {
			var namevaluearray = namevaluepair[i].split("=");
			if (namevaluearray[0] == "interactionId") {
				interactionId = JSON.parse(namevaluearray[1]);
				$('#interactionSuccessId').html(interactionId);
			}
		}
	};

});

function getBrowserId() {
	var
	aKeys = ["MSIE", "Firefox", "Safari", "Chrome", "Opera"],
	sUsrAg = navigator.userAgent,
	nIdx = aKeys.length - 1;
	for (nIdx; nIdx > -1 && sUsrAg.indexOf(aKeys[nIdx]) === -1; nIdx--);
	return nIdx;
}

$.urlParam = function(name) {
	var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
	if (results == null) {
		return null;
	} else {
		return decodeURI(results[1]) || 0;
	}
}

if (num != -1) {
	urlParams = new URLSearchParams(window.location.search);
	documentId = urlParams.get('documentId');

} else if (num == -1) {
	documentId = $.urlParam('documentId');
}

if (!documentId) {
	window.location.assign('/content/enach/welcome/login/create.html');
} else {
	$('.redcolor').html("Your E-NACH application with DOCUMENT NO. "+documentId+" has been Successfully submitted.");
}