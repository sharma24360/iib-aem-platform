package com.iib.platform.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.iib.platform.services.config.BillDeskResponseServletConfig;

/**
 * BillDeskResponse Servlet
 *
 * @author TechChefz (TCZ Consulting LLP)
 *
 */
@Component(
		service = { Servlet.class }, 
		name = "BillDeskResponse",
		enabled = true,
		property = {
				Constants.SERVICE_DESCRIPTION + "=" + "BillDeskResponse Servlet",
				"sling.servlet.methods=" + HttpConstants.METHOD_GET,
				"sling.servlet.paths="+ "/bin/billDeskResponse"
		})
@Designate(ocd = BillDeskResponseServletConfig.class)
public class BillDeskResponse extends SlingAllMethodsServlet {

	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(BillDeskResponse.class);

	private String sorryUrl;
	private String thankyouUrl;

	@Activate
	protected void activate(BillDeskResponseServletConfig config) {
		log.info("BillDesk Response has been activated!");

		sorryUrl = config.getSorryPageUrl();
		thankyouUrl = config.getThankYouPageUrl();
	}

	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {

		PrintWriter out = response.getWriter();
		try {
			String msg = "";

			if(request.getParameter("msg") != null)
				msg = request.getParameter("msg");

			log.info("message : " + msg);
			String[] test= msg.split("\\|");

			for(String bb : test) {
				log.info("Test : "+bb);
			}
			String AuthStatus = test[14];
			String txnRefNo = test[2];
			String bankRefNo = test[3];
			String txnAmt = test[17];
			log.info("{} {} {} {}",AuthStatus,txnAmt, txnRefNo, bankRefNo);

			if(AuthStatus.equals("0300"))
				response.sendRedirect(thankyouUrl +"?AuthStatus="+ AuthStatus+"&txnAmt="+txnAmt+"&txnRefNo="+txnRefNo+"&bankRefNo="+bankRefNo);
			else
				response.sendRedirect(sorryUrl +"?AuthStatus="+ AuthStatus+"&txnAmt="+txnAmt+"&txnRefNo="+txnRefNo+"&bankRefNo="+bankRefNo);
		}
		catch(Exception e) {
			log.error("{}",e);
			out.println(e.getMessage());
		}

	}
}

