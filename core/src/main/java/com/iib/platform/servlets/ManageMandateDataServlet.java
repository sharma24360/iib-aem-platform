package com.iib.platform.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Property;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.json.JSONArray;
import org.json.JSONObject;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.iib.platform.services.HttpAPIService;
import com.iib.platform.services.SoapAPIService;

/**
 * Mandate Data Servlet 
 *
 * @author TechChefz (TCZ Consulting LLP)
 *
 */
@Component(
		service = { Servlet.class }, 
		name = "Mandate Data Servlet",
		enabled = true,
		property = {
				Constants.SERVICE_DESCRIPTION + "=" + "Mandate Data Servlet",
				"sling.servlet.methods=" + HttpConstants.METHOD_POST,
				"sling.servlet.paths="+ "/bin/manageMandateData"
		})
public class ManageMandateDataServlet extends SlingAllMethodsServlet {

	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(ManageMandateDataServlet.class);
	
	@Reference
	private ResourceResolverFactory resourceResolverFactory;

	ResourceResolver resourceResolver;

	Session session;
	
	@Reference
	HttpAPIService httpAPIService;
	
	@Reference
	SoapAPIService soapApiService;
	
	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {	
		
		PrintWriter out = response.getWriter();
		Cookie[] cookies = request.getCookies();

		String mobileNumber = "";
		String dob = "";
		String mandateflag = "";
		JSONObject accounts = new JSONObject();
		JSONArray mandateData = new JSONArray();
		JSONObject res = new JSONObject();
		Value value = null;
		String mandateNodeData = "";

		try {
			for(Cookie cookie : cookies){
			    if("userMobNo".equals(cookie.getName())){
			    	mobileNumber = cookie.getValue();
			    }
			    if("userDob".equals(cookie.getName())) {
					dob = cookie.getValue();
				}
				if("flag".equals(cookie.getName())) {
					mandateflag = cookie.getValue();
				}
			}
			
			log.info(mandateflag + " :: Mandate Flag");
			
			// For Specific Mandate Accounts
			/*if(mandateflag.equalsIgnoreCase("true")) {
				JSONArray jsonArray = soapApiService.getAccountDetails(mobileNumber);
				String tempDate = "";
				JSONArray accountsFromDOB = new JSONArray();
				JSONArray accountsArray = new JSONArray();
				
				for(int i=0; i<jsonArray.length(); i++) {
					JSONObject tempJson = jsonArray.getJSONObject(i);
					tempDate = tempJson.getString("DOB");
					if(tempDate.equals(dob)) {
						accountsFromDOB.put(tempJson);
					}
				}
				
				for(int i=0; i<accountsFromDOB.length(); i++) {
					JSONObject tempJson = accountsFromDOB.getJSONObject(i);
					accountsArray.put(tempJson.getString("Account_Number"));
					log.info(accountsArray.get(i) + " :: Acc No");
					
					accounts = (JSONObject) httpAPIService.getviaAccountNo(mobileNumber).get("accounts");
					mandateData.put(accounts.get("mandateData"));
					
				}
			}*/
		
			accounts = (JSONObject) httpAPIService.getviaAccountNo(mobileNumber).get("accounts");
			mandateData = (JSONArray) accounts.get("mandateData");

			
			//Store MandateData into Node
			Map<String,Object> paramMap = new HashMap<String,Object>();
			paramMap.put(ResourceResolverFactory.SUBSERVICE, "replicationService");
			resourceResolver = resourceResolverFactory.getServiceResourceResolver(paramMap);
			session = resourceResolver.adaptTo(Session.class);
			
			Node rootNode = session.getRootNode();
			Node userNode = rootNode.getNode("apps/users");
			Boolean flag = false;
			
			NodeIterator nodeIterator = userNode.getNodes();
			while(nodeIterator.hasNext()) {
				Node tempNode = nodeIterator.nextNode();
				if(mobileNumber.equalsIgnoreCase(tempNode.getName())) {
					flag = true;
					break;
				} else {
					flag = false;
				}
			}
			if(flag==true) {
				Node currentNode = userNode.getNode(mobileNumber);
				if(currentNode.hasProperty("mandateData")) {
					log.info("Updating Property..");
					currentNode.getProperty("mandateData").remove();
					resourceResolver.commit();
					
					currentNode.setProperty("mandateData", mandateData.toString());
					Property mandateProperty = currentNode.getProperty("mandateData");
					value = mandateProperty.getValue();
					mandateNodeData = value.getString();
					resourceResolver.commit();
					
					res.put("success", "true");
					res.put("mandate", mandateNodeData);
				} else {
					log.info("Creating new property...");
					currentNode.setProperty("mandateData", mandateData.toString());
					Property mandateProperty = currentNode.getProperty("mandateData");
					value = mandateProperty.getValue();
					mandateNodeData = value.getString();
					
					res.put("success", "true");
					res.put("mandate", mandateNodeData);
				}
			} else {
				res.put("success", "false");
			}
			session.save();
			out.println(res);
			response.flushBuffer();
			
		} catch(Exception e) {
			out.println("Exception in Mandate Data Servlet :: " + e.getMessage());
		}	
	}
}
