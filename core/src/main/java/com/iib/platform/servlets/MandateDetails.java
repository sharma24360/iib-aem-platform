package com.iib.platform.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.json.JSONObject;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(
		service = { Servlet.class }, 
		name = "MandateDetailsServlet",
		enabled = true,
		property = {
				Constants.SERVICE_DESCRIPTION + "=" + "MandateDetails Servlet",
				"sling.servlet.methods=" + HttpConstants.METHOD_POST,
				"sling.servlet.paths="+ "/bin/getMandateDetails"
})
public class MandateDetails extends SlingAllMethodsServlet {
	
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(MandateDetails.class);
	
	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		String umrnNumber = request.getParameter("umrnNumber");
		String url = "https://ibluatapig.indusind.com/app/uat/emandateFlux/emandate";
		
		try {
			
			JSONObject requestObject = new JSONObject();
			
			JSONObject requestRoot = new JSONObject();
			JSONObject emandateObj = new JSONObject();
			emandateObj.put("umrn", umrnNumber); 
			requestRoot.put("emandateRoot", emandateObj);

			JSONObject requestHeaderRoot = new JSONObject();
			requestHeaderRoot.put("mndAction", "GetEmandateByUMRN");
			requestHeaderRoot.put("sourceId", "1002");
			requestHeaderRoot.put("msgId", "msg000011");
			
			requestObject.put("request", requestRoot);
			requestObject.put("header", requestHeaderRoot);
			
			HttpClient httpclient = HttpClientBuilder.create().build();
			HttpPost httpPostRequest = new HttpPost(url);

			StringEntity se = new StringEntity(requestObject.toString());
		
			httpPostRequest.setEntity(se);
			httpPostRequest.setHeader("X-IBM-Client-Secret","F8fF4nK5bY4aE3dQ7uW1jY7hP7bI1sY5qW0hD6tJ5kH0iX4oO1");
			httpPostRequest.setHeader("X-IBM-Client-Id","020939a3-017d-40d0-b011-511c2f52631b");
			httpPostRequest.setHeader("mndAction","GetEmandateByUMRN");
			httpPostRequest.setHeader("sourceId","1002");
			httpPostRequest.setHeader("msgId","msg000011");
			httpPostRequest.setHeader("Content-Type","application/json");
			
			HttpResponse resp = (HttpResponse) httpclient.execute(httpPostRequest);
			HttpEntity entity = resp.getEntity();

			//Read the content stream
			InputStream instream = entity.getContent();
			BufferedReader br =  new BufferedReader(new InputStreamReader(instream));

			StringBuilder sb = new StringBuilder();
			String line;
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
			
			JSONObject jsonRes = new JSONObject(sb.toString());
			out.println(jsonRes);
			
			
		} catch(Exception e) {
			log.error("Exception in MandateDetailsServlet :: " + e.getMessage());
		}
	
	}
}
