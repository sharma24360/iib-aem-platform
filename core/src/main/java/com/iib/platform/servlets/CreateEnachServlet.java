package com.iib.platform.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Property;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.json.JSONObject;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Create E-nach Servlet 
 *
 * @author TechChefz (TCZ Consulting LLP)
 *
 */
@Component(
		service = { Servlet.class }, 
		name = "Create E-NACH Servlet",
		enabled = true,
		property = {
				Constants.SERVICE_DESCRIPTION + "=" + "Create e-NACH Servlet",
				"sling.servlet.methods=" + HttpConstants.METHOD_POST,
				"sling.servlet.paths="+ "/bin/createEnach"
		})
public class CreateEnachServlet extends SlingAllMethodsServlet {

	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(CreateEnachServlet.class);
	
	@Reference
	private ResourceResolverFactory resourceResolverFactory;

	ResourceResolver resourceResolver;

	Session session;
	
	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		Cookie[] cookies = request.getCookies();
		
		String mobileNumber = "";
		JSONObject res = new JSONObject();
		Value value = null;
		String cifDetails = "";

		try {
			for(Cookie cookie : cookies){
			    if("userMobNo".equals(cookie.getName())){
			    	mobileNumber = cookie.getValue();
			    }
			}
			
			Map<String,Object> paramMap = new HashMap<String,Object>();
			paramMap.put(ResourceResolverFactory.SUBSERVICE, "replicationService");
			resourceResolver = resourceResolverFactory.getServiceResourceResolver(paramMap);
			session = resourceResolver.adaptTo(Session.class);
			
			Node rootNode = session.getRootNode();
			Node userNode = rootNode.getNode("apps/users");
			Boolean flag = false;
			
			NodeIterator nodeIterator = userNode.getNodes();
			while(nodeIterator.hasNext()) {
				Node tempNode = nodeIterator.nextNode();
				if(mobileNumber.equalsIgnoreCase(tempNode.getName())) {
					flag = true;
					break;
				} else {
					flag = false;
				}
			}
			if(flag==true) {
				Node currentNode = userNode.getNode(mobileNumber);
				Property mandateProperty = currentNode.getProperty("CIFDetails");
				value = mandateProperty.getValue();
				cifDetails = value.getString();
				res.put("success", "true");
				res.put("cifDetails", cifDetails);
			} else {
				res.put("success", "false");
			}
			session.save();
			out.println(res);
			response.flushBuffer();
			
		} catch(Exception e) {
			log.info("Exception in Create E-NACH Servlet");
			
		}
		

	}
}
