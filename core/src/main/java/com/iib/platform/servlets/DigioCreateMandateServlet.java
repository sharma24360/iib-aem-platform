package com.iib.platform.servlets;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.json.JSONArray;
import org.json.JSONObject;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.CDATASection;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.InputSource;

import com.iib.platform.services.config.DigioCreateMandateServletConfig;

/**
 * DigioCreateMandate Servlet
 *
 * @author TechChefz (TCZ Consulting LLP)
 *
 */
@Component(
		immediate = true,
		service = { Servlet.class }, 
		enabled = true,
		property = {
				Constants.SERVICE_DESCRIPTION + "=" + "DigioCreateMandate Servlet",
				"sling.servlet.methods=" + HttpConstants.METHOD_POST,
				"sling.servlet.paths="+ "/bin/digioCreateMandate"
		})
@Designate(ocd=DigioCreateMandateServletConfig.class)
public class DigioCreateMandateServlet extends SlingAllMethodsServlet {

	private static final long serialVersionUID = 1L;

	private static Logger log = LoggerFactory.getLogger(DigioCreateMandateServlet.class);

	private String endPointUrl;

	private String clientSecretKey;
	private String clientSecretValue;
	private String clientIdKey;
	private String clientIdValue;

	/**Request Body Parameters*/
	private String accountNo;
	private String cifId;
	private String callRelatedTo;
	private String callType;
	private String callSubType;
	private String media;
	private String interactionState;
	private String team;
	private String subject;
	private String status;
	private String message;
	private String smsFlag;
	private String emailFlag;

	/**API Properties*/
	private String customerNameF1;
	private String destinationBankF2;
	private String customerAccNoF3;
	private String confirmAccNoF4;
	private String destinationBankIdF5;
	private String maxAmountF6;
	private String transferFreqF7;
	private String startDateF8;
	private String endDateF9;
	private String totalAmountF10;
	private String totalInstallmentsF11;
	private String referralCodeF12;
	private String aadharNoF13;
	private String creditAccountF14;
	private String mobileF15;

	/**Digio API Properties*/
	private String digioUrl;
	private String sponserBankId;
	private String sponserBankName;
	private String bankIdentifier;
	private String loginId;
	private String mandateSequence;
	private String customerAcType;
	private String managementCategory;
	private String serviceProviderName;
	private String serviceProviderUtility;
	private String instrumentType;
	private String isRecurring;
	private String enachType;
	private String authorization;
	
	private String interactionId;

	@Activate
	public void activate(DigioCreateMandateServletConfig config) {
		log.info("Activated DigioCreateMandateServlet");

		this.endPointUrl = config.getEndPointUrl();
		this.clientSecretKey = config.getClientSecretKey();
		this.clientSecretValue = config.getClientSecretValue();
		this.clientIdKey = config.getClientID();
		this.clientIdValue = config.getClientIDValue();

		this.accountNo = config.accountNo();
		this.cifId = config.cifId();
		this.callRelatedTo = config.callRelatedTo();
		this.callType = config.callType();
		this.callSubType = config.callSubType();
		this.media = config.media();
		this.interactionState = config.interactionState();
		this.team = config.team();
		this.subject = config.subject();
		this.status = config.status();
		this.message = config.message();
		this.smsFlag = config.smsFlag();
		this.emailFlag = config.emailFlag();

		this.customerNameF1 = config.f1();
		this.destinationBankF2 = config.f2();
		this.customerAccNoF3 = config.f3();
		this.confirmAccNoF4 = config.f4();
		this.destinationBankIdF5 = config.f5();
		this.maxAmountF6 = config.f6();
		this.transferFreqF7 = config.f7();
		this.startDateF8 = config.f8();
		this.endDateF9 = config.f9();
		this.totalAmountF10 = config.f10();
		this.totalInstallmentsF11 = config.f11();
		this.referralCodeF12 = config.f12();
		this.aadharNoF13  = config.f13();
		this.creditAccountF14 = config.f14();
		this.mobileF15 = config.f15();

		this.digioUrl = config.getDigioUrl();
		this.sponserBankId = config.sponserBankId();
		this.sponserBankName = config.sponserBankName();
		this.bankIdentifier = config.bankIdentifier();
		this.loginId = config.loginId();
		this.mandateSequence = config.mandateSequence();
		this.customerAcType = config.customerAcType();
		this.managementCategory = config.managementCategory();
		this.serviceProviderName = config.serviceProviderName();
		this.serviceProviderUtility = config.serviceProdividerUtility();
		this.instrumentType = config.instrumentType();
		this.isRecurring = config.isRecurring();
		this.enachType = config.enachType();
		this.authorization = config.authorization();
	}

	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		String destinationBank = request.getParameter("destinationBank");
		String destinationBankId = request.getParameter("destinationBankId");
		String customerAccountNo = request.getParameter("customerAccountNo");
		String maximum_amount = request.getParameter("maximum_amount");
		String transferFrequency = request.getParameter("transferFrequency");
		String mobileNo = request.getParameter("mobileNo");
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		String customer_name = request.getParameter("customerName");
		String customerRefNumber = request.getParameter("accountNo");
		String confirmAccountNo = request.getParameter("confirmAccountNo");
		String totalAmount = request.getParameter("totalAmount");
		String totalInstallments = request.getParameter("totalInstallments");
		String referralCode = request.getParameter("referralCode");
		String aadharNo = request.getParameter("aadharNo");

		/**
		 * Labels
		 */
		String customerNameLabel = request.getParameter("customerNameLabel");
		String bankNameLabel = request.getParameter("bankNameLabel");
		String accountNumberLabel = request.getParameter("accountNumberLabel");
		String confirmAccountLabel = request.getParameter("confirmAccountLabel");
		String ifscLabel = request.getParameter("ifscLabel");
		String maximumAmountLabel = request.getParameter("maximumAmountLabel");
		String transferFreqLabel = request.getParameter("transferFreqLabel");
		String startDateLabel = request.getParameter("startDateLabel");
		String endDateLabel = request.getParameter("endDateLabel");
		String totalAmountLabel = request.getParameter("totalAmountLabel");
		String totalInstallmentsLabel = request.getParameter("totalInstallmentsLabel");
		String aadharLabel = request.getParameter("aadharLabel");
		String referralCodeLabel = request.getParameter("referralCodeLabel");
		String creditAccountLabel = request.getParameter("creditAccountLabel");

		try {
			/**
			 * Help Me API Integration
			 */
			final String PREFERRED_PREFIX = "soap";
			MessageFactory messageFactory = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL);
			SOAPMessage soapMessage = messageFactory.createMessage();
			String prefix = "tem";
			String uri = "http://tempuri.org/";
			SOAPEnvelope envelope = soapMessage.getSOAPPart().getEnvelope();
			SOAPHeader soapHeader = soapMessage.getSOAPHeader();
			envelope.removeNamespaceDeclaration(envelope.getPrefix());
			envelope.setPrefix(PREFERRED_PREFIX);
			envelope.addNamespaceDeclaration(prefix, uri);
			soapHeader.setPrefix(PREFERRED_PREFIX);
			SOAPBody soapBody = soapMessage.getSOAPBody();
			soapBody.setPrefix(PREFERRED_PREFIX);

			DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = documentFactory.newDocumentBuilder();
			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("Talisma");

			Element talismaPropertiesDetails = doc.createElement("TalismaPropertiesDetails");
			rootElement.appendChild(talismaPropertiesDetails);

			Text custNameNode = doc.createTextNode(customerNameLabel+"--"+customer_name);
			Element property1 = doc.createElement("Property");
			String idKey1 = "ID";
			String idValue1 = customerNameF1;
			property1.setAttribute(idKey1, idValue1);
			property1.appendChild(custNameNode);
			talismaPropertiesDetails.appendChild(property1);

			Text bankNameNode = doc.createTextNode(bankNameLabel+"--"+destinationBank);
			Element property2 = doc.createElement("Property");
			String idKey2 = "ID";
			String idKeyValue2 = destinationBankF2;
			property2.setAttribute(idKey2, idKeyValue2);
			property2.appendChild(bankNameNode);
			talismaPropertiesDetails.appendChild(property2);


			Text customerAccountNode = doc.createTextNode(accountNumberLabel+"--"+customerAccountNo);
			Element property3 = doc.createElement("Property");
			String idKey3 = "ID";
			String idKeyValue3 = customerAccNoF3;
			property3.setAttribute(idKey3, idKeyValue3);
			property3.appendChild(customerAccountNode);
			talismaPropertiesDetails.appendChild(property3);

			Text confirmAccountNode = doc.createTextNode(confirmAccountLabel+"--"+confirmAccountNo);
			Element property4 = doc.createElement("Property");
			String idKey4 = "ID";
			String idKeyValue4 = confirmAccNoF4;
			property4.setAttribute(idKey4, idKeyValue4);
			property4.appendChild(confirmAccountNode);
			talismaPropertiesDetails.appendChild(property4);

			Text ifscNode = doc.createTextNode(ifscLabel+"--"+destinationBankId);
			Element property5 = doc.createElement("Property");
			String idKey5 = "ID";
			String idKeyValue5 = destinationBankIdF5;
			property5.setAttribute(idKey5, idKeyValue5);
			property5.appendChild(ifscNode);
			talismaPropertiesDetails.appendChild(property5);

			Text maximumAmountNode = doc.createTextNode(maximumAmountLabel+"--"+maximum_amount);
			Element property6 = doc.createElement("Property");
			String idKey6 = "ID";
			String idKeyValue6 = maxAmountF6;
			property6.setAttribute(idKey6, idKeyValue6);
			property6.appendChild(maximumAmountNode);
			talismaPropertiesDetails.appendChild(property6);

			Text transferFreqNode = doc.createTextNode(transferFreqLabel+"--"+transferFrequency);
			Element property7 = doc.createElement("Property");
			String idKey7 = "ID";
			String idKeyValue7 = transferFreqF7;
			property7.setAttribute(idKey7, idKeyValue7);
			property7.appendChild(transferFreqNode);
			talismaPropertiesDetails.appendChild(property7);

			Text startDateNode = doc.createTextNode(startDateLabel+"--"+startDate);
			Element property8 = doc.createElement("Property");
			String idKey8 = "ID";
			String idKeyValue8 = startDateF8;
			property8.setAttribute(idKey8, idKeyValue8);
			property8.appendChild(startDateNode);
			talismaPropertiesDetails.appendChild(property8);

			Text endDateNode = doc.createTextNode(endDateLabel+"--"+endDate);
			Element property9 = doc.createElement("Property");
			String idKey9 = "ID";
			String idKeyValue9 = endDateF9;
			property9.setAttribute(idKey9, idKeyValue9);
			property9.appendChild(endDateNode);
			talismaPropertiesDetails.appendChild(property9);

			Text totalAmountNode = doc.createTextNode(totalAmountLabel+"--"+totalAmount);
			Element property10 = doc.createElement("Property");
			String idKey10 = "ID";
			String idKeyValue10 = totalAmountF10;
			property10.setAttribute(idKey10, idKeyValue10);
			property10.appendChild(totalAmountNode);
			talismaPropertiesDetails.appendChild(property10);

			Text totalInstallmentsNode = doc.createTextNode(totalInstallmentsLabel+"--"+totalInstallments);
			Element property11 = doc.createElement("Property");
			String idKey11 = "ID";
			String idKeyValue11 = totalInstallmentsF11;
			property11.setAttribute(idKey11, idKeyValue11);
			property11.appendChild(totalInstallmentsNode);
			talismaPropertiesDetails.appendChild(property11);

			Text referralCodeNode = doc.createTextNode(referralCodeLabel+"--"+referralCode);
			Element property12 = doc.createElement("Property");
			String idKey12 = "ID";
			String idKeyValue12 = referralCodeF12;
			property12.setAttribute(idKey12, idKeyValue12);
			property12.appendChild(referralCodeNode);
			talismaPropertiesDetails.appendChild(property12);

			Text aadharNode = doc.createTextNode(aadharLabel+"--"+aadharNo);
			Element property13 = doc.createElement("Property");
			String idKey13 = "ID";
			String idKeyValue13 = aadharNoF13;
			property13.setAttribute(idKey13, idKeyValue13);
			property13.appendChild(aadharNode);
			talismaPropertiesDetails.appendChild(property13);

			Text creditAccountNode = doc.createTextNode(creditAccountLabel+"--"+customerRefNumber);
			Element property14 = doc.createElement("Property");
			String idKey14 = "ID";
			String idKeyValue14 = creditAccountF14;
			property14.setAttribute(idKey14, idKeyValue14);
			property14.appendChild(creditAccountNode);
			talismaPropertiesDetails.appendChild(property14);

			Text mobileNode = doc.createTextNode(mobileNo);
			Element property15 = doc.createElement("Property");
			String idKey15 = "ID";
			String idKeyValue15 = mobileF15;
			property15.setAttribute(idKey15, idKeyValue15);
			property15.appendChild(mobileNode);
			talismaPropertiesDetails.appendChild(property15);

			doc.appendChild(rootElement);

			DOMSource domSource = new DOMSource(doc);
			StringWriter writer = new StringWriter();
			StreamResult result = new StreamResult(writer);
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.transform(domSource, result);
			String innerXml = writer.toString();

			SOAPBodyElement createServiceRequest = soapBody.addBodyElement(envelope.createQName("CreateServiceRequest", prefix));

			SOAPElement accountNo = createServiceRequest.addChildElement(envelope.createQName("AccountNo", prefix));
			accountNo.addTextNode(this.accountNo);

			SOAPElement cif_id = createServiceRequest.addChildElement(envelope.createQName("CIF_ID", prefix));
			cif_id.addTextNode(cifId);

			SOAPElement callRelatedTo = createServiceRequest.addChildElement(envelope.createQName("CallRelatedTo", prefix));
			callRelatedTo.addTextNode(this.callRelatedTo);

			SOAPElement callType = createServiceRequest.addChildElement(envelope.createQName("CallType", prefix));
			callType.addTextNode(this.callType);

			SOAPElement callSubType = createServiceRequest.addChildElement(envelope.createQName("CallSubType", prefix));
			callSubType.addTextNode(this.callSubType);

			SOAPElement media = createServiceRequest.addChildElement(envelope.createQName("Media", prefix));
			media.addTextNode(this.media);

			SOAPElement interactionState = createServiceRequest.addChildElement(envelope.createQName("InteractionState", prefix));
			interactionState.addTextNode(this.interactionState);

			SOAPElement team = createServiceRequest.addChildElement(envelope.createQName("Team", prefix));
			team.addTextNode(this.team);

			SOAPElement subject = createServiceRequest.addChildElement(envelope.createQName("Subject", prefix));
			subject.addTextNode(this.subject);

			SOAPElement message = createServiceRequest.addChildElement(envelope.createQName("Message", prefix));
			message.addTextNode(this.message);

			SOAPElement status = createServiceRequest.addChildElement(envelope.createQName("Status", prefix));
			status.addTextNode(this.status);

			SOAPElement sms_flag = createServiceRequest.addChildElement(envelope.createQName("SMS_Flag", prefix));
			sms_flag.addTextNode(this.smsFlag);

			SOAPElement email_flag = createServiceRequest.addChildElement(envelope.createQName("Email_Flag", prefix));
			email_flag.addTextNode(this.emailFlag);

			SOAPElement xml_str = createServiceRequest.addChildElement(envelope.createQName("XML_Str", prefix));
			CDATASection cdata = xml_str.getOwnerDocument().createCDATASection(innerXml);
			xml_str.appendChild(cdata);

			if(StringUtils.isNotBlank(clientIdValue)) {
				soapMessage.getMimeHeaders().addHeader(clientSecretKey, clientSecretValue);
				soapMessage.getMimeHeaders().addHeader(clientIdKey, clientIdValue);
			} else {
				log.info("Blank Headers");
			}
			soapMessage.saveChanges();

			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory.createConnection();
			URL endUrl = new URL(endPointUrl);
			SOAPMessage soapResponse = soapConnection.call(soapMessage, endUrl);

			String responseXml = "";
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			soapResponse.writeTo(stream);
			responseXml = stream.toString();

			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = factory.newDocumentBuilder();
			Document document = documentBuilder.parse(new InputSource(new StringReader(responseXml)));

			Map<String, String> resultParam = new HashMap<>();
			NodeList nodeList = document.getElementsByTagName("CreateServiceRequestResponse");
			for(int i=0; i<nodeList.getLength(); i++) {
				Node node = nodeList.item(i);
				if(node.getNodeType() == Node.ELEMENT_NODE) {
					Element element = (Element) node;
					NodeList childNode = element.getChildNodes();
					for(int j=0; j<childNode.getLength(); j++) {
						Node eachChild = childNode.item(j);
						Element eachElement = (Element) eachChild;
						if(eachElement.getTagName() == "CreateServiceRequestResult") {
							resultParam.put("status", eachElement.getTextContent());
						}
						else if(eachElement.getTagName() == "InteractionID") {
							resultParam.put("interactionId",eachElement.getTextContent());
						}
					}
				}
			}
			
			this.interactionId = resultParam.get("interactionId");
			// Setting Interaction ID in Cookie
			Cookie interactionCookie = new Cookie("interactionId", this.interactionId);
			interactionCookie.setMaxAge(60 * 24 * 3600);
			interactionCookie.setPath("/");
			response.addCookie(interactionCookie);
			log.info(interactionId + " :: Interaction ID");
			
			/**
			 *	Help ME API End 
			 */

			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.ENGLISH);
			Date date = new Date();
			String currentDate = dateFormat.format(date);

			String alphaNumeric = RandomStringUtils.randomAlphanumeric(32).toUpperCase();
			JSONObject postData = new JSONObject();

			JSONObject content = new JSONObject();
			content.put("first_collection_date", startDate);
			content.put("final_collection_date", endDate); 
			content.put("mandate_request_id", alphaNumeric); 
			content.put("mandate_creation_date_time", currentDate);
			content.put("sponsor_bank_id",sponserBankId); 
			content.put("sponsor_bank_name", sponserBankName);
			content.put("destination_bank_id",destinationBankId);
			content.put("destination_bank_name", destinationBank); 
			content.put("bank_identifier", bankIdentifier); 
			content.put("login_id", loginId);
			content.put("mandate_sequence", mandateSequence);
			content.put("customer_account_type", customerAcType);
			content.put("management_category", managementCategory); 
			content.put("service_provider_name", serviceProviderName);
			content.put("service_provider_utility_code", serviceProviderUtility);
			content.put("customer_account_number", customerAccountNo); 
			content.put("instrument_type", instrumentType); 
			content.put("customer_name", customer_name);
			content.put("customer_mobile", mobileNo); 
			content.put("customer_phone", mobileNo); 
			content.put("maximum_amount", maximum_amount); 
			content.put("is_recurring", isRecurring);
			content.put("frequency", transferFrequency); 
			content.put("customer_ref_number", customerRefNumber);
			
			JSONArray signers = new JSONArray();
			JSONObject identifiers = new JSONObject();
			identifiers.put("identifier", mobileNo);
			signers.put(identifiers);

			postData.put("signers", signers);
			postData.put("expire_in_days", 30);
			postData.put("enach_type", enachType);
			postData.put("content", content.toString());
			
			log.info("Digio Create Request :: " + postData.toString());

			HttpClient httpclient = HttpClientBuilder.create().build();
			HttpPost httpPostRequest = new HttpPost(digioUrl);

			StringEntity se = new StringEntity(postData.toString());

			httpPostRequest.setEntity(se);
			httpPostRequest.setHeader("Authorization", authorization);
			httpPostRequest.setHeader("Content-Type","application/json");
			HttpResponse resp = (HttpResponse) httpclient.execute(httpPostRequest);
			HttpEntity entity = resp.getEntity();

			//Read the content stream
			InputStream instream = entity.getContent();
			BufferedReader br =  new BufferedReader(new InputStreamReader(instream));

			StringBuilder sb = new StringBuilder();
			String line;
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}

			JSONObject jsonRes = new JSONObject(sb.toString());
			out.println(jsonRes);

		} catch(Exception e) {
			log.error("Exception in DigioCreateMandateServlet :: " + e.getMessage());
		}

	}
}
