package com.iib.platform.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.json.JSONObject;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.iib.platform.api.request.RequestObject;
import com.iib.platform.api.response.ResponseBody;
import com.iib.platform.api.response.ResponseObject;
import com.iib.platform.services.HttpAPIService;
import com.iib.platform.services.StoreOTPService;
import com.iib.platform.services.config.GenerateOTPConfig;

/**
 * Generate OTP Servlet
 *
 * @author TechChefz (TCZ Consulting LLP)
 *
 */

@Component(
		immediate = true,
		service = { Servlet.class }, 
		property = {
				Constants.SERVICE_DESCRIPTION + "=" + "Generate OTP Servlet",
				"sling.servlet.methods=" + HttpConstants.METHOD_GET,
				"sling.servlet.paths="+ "/bin/generateOtp"
		})
@Designate(ocd=GenerateOTPConfig.class)
public class GenerateOTP extends SlingSafeMethodsServlet {

	private static final long serialVersionUID = 1L;
	
	/**Static Logger*/
	private static Logger log = LoggerFactory.getLogger(GenerateOTP.class);

	@Reference
	private HttpAPIService httpAPIService;
	
	@Reference
	StoreOTPService storeOTPService;
	
	/**Private Static Fields*/
	private String requestUrl;
	
	@Activate
	public void activate(GenerateOTPConfig config) {
		log.info("Activated GenerateOTP");
		
		this.requestUrl = config.requestUrl();
	}

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {

		PrintWriter out = response.getWriter();
		String mobile = request.getParameter("mobile");
		
		try {
			int otp = (int) (Math.floor(Math.random() * 900000) + 100000);
			
			storeOTPService.storeOTP(mobile, otp);
			
			RequestObject requestObject = new RequestObject();
			requestObject.setRequestUrl(requestUrl);
			ResponseObject responseObject = httpAPIService.getSMSResponse(requestObject, mobile, otp);
			ResponseBody body = responseObject.getResponseBody();
			String json = body.getResponseContentXML();		
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("TID", json);			
			out.println(jsonObj);
			response.flushBuffer();

		}
		catch(Exception e) {
			log.error("Exception in Generate OTP Servlet :: " + e.getMessage());
		}

	}
}