package com.iib.platform.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.json.JSONArray;
import org.json.JSONObject;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.iib.platform.services.config.DigioCancelMandateServletConfig;

/**
 * DigioCancelMandate Servlet
 *
 * @author TechChefz (TCZ Consulting LLP)
 *
 */
@Component(
		service = { Servlet.class }, 
		name = "DigioCancelMandateServlet",
		enabled = true,
		property = {
				Constants.SERVICE_DESCRIPTION + "=" + "DigioCancelMandate Servlet",
				"sling.servlet.methods=" + HttpConstants.METHOD_POST,
				"sling.servlet.paths="+ "/bin/digioCancelMandate"
		})
@Designate(ocd=DigioCancelMandateServletConfig.class)
public class DigioCancelMandateServlet extends SlingAllMethodsServlet {

	private static final long serialVersionUID = 1L;
	
	/**Static Logger*/
	private static Logger log = LoggerFactory.getLogger(DigioCreateMandateServlet.class);
	
	private String sponserBankId;
	private String urllink;
	private String authorization;
	
	
	@Activate
	public void activate(DigioCancelMandateServletConfig config) {
		log.info("Activated DigioCancelMandateServlet!");
		
		this.sponserBankId = config.getSponserBankId();
		this.urllink=config.geturl();
		this.authorization = config.authorization();
	}
	
	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		
		String destinationBank = request.getParameter("destinationBank");
		String destinationBankId = request.getParameter("destinationBankId");
		String mobile = request.getParameter("mobile");
		String originalMandateId = request.getParameter("mandate_id");
		
		try {
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.ENGLISH);
			Date date = new Date();
			String currentDate = dateFormat.format(date);
			String alphaNumeric = RandomStringUtils.randomAlphanumeric(32).toUpperCase();
			
			JSONObject postData = new JSONObject();

			JSONObject content = new JSONObject();
			content.put("mandate_request_id", alphaNumeric); 
			content.put("mandate_creation_date_time", currentDate);
			content.put("sponsor_bank_id", sponserBankId); 
			content.put("sponsor_bank_name", "INDUSIND BANK LTD");
			content.put("destination_bank_id",destinationBankId);
			content.put("destination_bank_name", destinationBank); 
			content.put("bank_identifier", "INDB"); 
			content.put("login_id","INDB00160000010226");
			content.put("mandate_sequence", "001");
			content.put("original_mandate_id", originalMandateId);
			content.put("cancellation_reason", "C001");
						
			JSONArray signers = new JSONArray();
			JSONObject identifiers = new JSONObject();
			identifiers.put("identifier", mobile);
			signers.put(identifiers);
			
			postData.put("signers", signers);
			postData.put("comment", "Please sign the document");
			postData.put("expire_in_days", 10);
			postData.put("enach_type", "CANCEL");
			postData.put("partner_entity_email", "CANCEL");
			postData.put("content", content.toString());
			
			
			String url = urllink;
			
			HttpClient httpclient = HttpClientBuilder.create().build();
			HttpPost httpPostRequest = new HttpPost(url);
			StringEntity se = new StringEntity(postData.toString());

			httpPostRequest.setEntity(se);
			httpPostRequest.setHeader("Authorization", authorization);
			httpPostRequest.setHeader("Content-Type","application/json");
			HttpResponse resp = (HttpResponse) httpclient.execute(httpPostRequest);
			HttpEntity entity = resp.getEntity();

			//Read the content stream
			InputStream instream = entity.getContent();
			BufferedReader br =  new BufferedReader(new InputStreamReader(instream));

			StringBuilder sb = new StringBuilder();
			String line;
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
			
			JSONObject jsonRes = new JSONObject(sb.toString());
			out.println(jsonRes);
			
		} catch(Exception e) {
			log.error("Exception in DigioCancelMandateServlet :: " + e.getMessage());
		}
	
	}
}
