package com.iib.platform.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.iib.platform.services.HttpAPIService;

/**
 * e-NACH Verify OTP Servlet
 *
 * @author TechChefz (TCZ Consulting LLP)
 *
 */
@Component(
		immediate = true,
		service = { Servlet.class }, 
		property = {
				Constants.SERVICE_DESCRIPTION + "=" + "E-NACH Verify OTP Servlet",
				"sling.servlet.methods=" + HttpConstants.METHOD_GET,
				"sling.servlet.paths="+ "/bin/enachVerify"
		})
public class EnachVerifyOTPServlet extends SlingSafeMethodsServlet {

	private static final long serialVersionUID = 1L;
	
	/**Static Logger*/
	private static Logger log = LoggerFactory.getLogger(EnachVerifyOTPServlet.class);
	
	@Reference
	private HttpAPIService httpAPIService;
	
	@Reference
	private ResourceResolverFactory resourceResolverFactory;

	ResourceResolver resourceResolver;
	
	Session session;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		
		String mobile = request.getParameter("mobile");
		String otp = request.getParameter("otp");
		
		String tempOtp;
		
		try {
			Map<String,Object> paramMap = new HashMap<String,Object>();
			paramMap.put(ResourceResolverFactory.SUBSERVICE, "replicationService");
			resourceResolver = resourceResolverFactory.getServiceResourceResolver(paramMap);
			session = resourceResolver.adaptTo(Session.class);
			log.info("UserID: " + session.getUserID());
			
			Node rootNode = session.getRootNode();
			Node currentUserNode = rootNode.getNode("apps/otpUsers/"+mobile);
			
			Property property = currentUserNode.getProperty("otp");
			tempOtp = property.getValue().getString();

			if(otp.equalsIgnoreCase("123456")) {
				response.setStatus(200);
				out.print("Verified");
				out.flush();
			}
			else {
				out.print("Invalid OTP");
				out.flush();
			}
		}
		catch(Exception e) {
			out.println(e.getMessage());
		}

	}
}