package com.iib.platform.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Session;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.json.JSONArray;
import org.json.JSONObject;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.iib.platform.services.SoapAPIService;

@Component(
		service = { Servlet.class }, 
		name = "AccountDetails",
		enabled = true,
		property = {
				Constants.SERVICE_DESCRIPTION + "=" + "Account Details Servlet",
				"sling.servlet.methods=" + HttpConstants.METHOD_POST,
				"sling.servlet.paths="+ "/bin/accountDetails"
		})
public class EnachCIFAPIServlet extends SlingAllMethodsServlet {

	@Reference
	private ResourceResolverFactory resourceResolverFactory;

	ResourceResolver resourceResolver;
	
	Session session;

	@Reference
	SoapAPIService soapApiService;

	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(EnachCIFAPIServlet.class);

	@Activate
	public void activate() {
		log.info("E-NACH CIF API Servlet has been Activated!");
	}

	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {	
		PrintWriter out = response.getWriter();
		String mobileNumber = request.getParameter("mobile");

		try {
			JSONArray jsonArray = soapApiService.getAccountDetails(mobileNumber);
			JSONObject resObj;
			Boolean flag = null;

			Map<String,Object> paramMap = new HashMap<String,Object>();
			paramMap.put(ResourceResolverFactory.SUBSERVICE, "replicationService");
			resourceResolver = resourceResolverFactory.getServiceResourceResolver(paramMap);
			session = resourceResolver.adaptTo(Session.class);
			log.info("UserID: " + session.getUserID());

			Node rootNode = session.getRootNode();
			Node apps = rootNode.getNode("apps");
			Boolean nodeFlag = false;
			
			if(!jsonArray.isNull(0)) {
				if(!apps.hasNode("users")) {
					Node userNode = apps.addNode("users", "nt:unstructured");
					Node currentNode = userNode.addNode(mobileNumber,"nt:unstructured");
					currentNode.setProperty("CIFDetails", jsonArray.toString());
				} else {
					Node userNode = apps.getNode("users");
					NodeIterator nodeIterator = userNode.getNodes();
					while(nodeIterator.hasNext()) {
						Node currentNode = nodeIterator.nextNode();
						// Updating the CIFDetails Property in the existing Node.
						if(mobileNumber.equalsIgnoreCase(currentNode.getName())) {
							if(currentNode.hasProperty("CIFDetails")) {
								currentNode.getProperty("CIFDetails").remove();
								resourceResolver.commit();

								currentNode.setProperty("CIFDetails", jsonArray.toString());
								resourceResolver.commit();
							} else {
								currentNode.setProperty("CIFDetails", jsonArray.toString());
							}
							nodeFlag = false;
							break;
						} else {
							nodeFlag = true;
						}
					}
				}

				if(nodeFlag==true) {
					Node userNode = apps.getNode("users");
					Node newNode = userNode.addNode(mobileNumber,"nt:unstructured");
					newNode.setProperty("CIFDetails", jsonArray.toString());
				}
				session.save();

				Cookie cookie = new Cookie("userMobNo", mobileNumber);
				cookie.setMaxAge(60 * 24 * 3600);
				cookie.setPath("/");
				response.addCookie(cookie);

				String previousDob = "";
				String nextDob = "";

				JSONObject tempJsoni = new JSONObject();
				JSONObject tempJsonj = new JSONObject();

				for(int i=0; i<jsonArray.length(); i++) {
					tempJsoni = jsonArray.getJSONObject(i);
					previousDob = tempJsoni.getString("DOB");

					for(int j=0; j<jsonArray.length(); j++) {
						tempJsonj = jsonArray.getJSONObject(j);
						nextDob = tempJsonj.getString("DOB");

						if(previousDob.equalsIgnoreCase(nextDob)) {
							//log.info("Previous and Next are same: " + previousDob + " " + nextDob);
							flag = true;
						} else {
							//log.info("Previous and Next are not same: " + previousDob + " " + nextDob);
							flag = false;
							break;
						}
					}
				}
			} else {
				resObj = new JSONObject();
				resObj.put("success", "errorDetails");
				out.print(resObj);
				response.flushBuffer();
			}

			if(flag==true) {
				resObj = new JSONObject();
				resObj.put("success", "true");
				resObj.put("isDobRequired", "false");
				out.print(resObj);
				response.flushBuffer();
			} else {
				resObj = new JSONObject();
				resObj.put("success", "false");
				resObj.put("isDobRequired", "true");
				out.print(resObj);
				response.flushBuffer();
			} 
		}
		catch(Exception e) {
			log.info("Exception in e-NACH CIF API :: " + e.getMessage());
		}

	}
}
