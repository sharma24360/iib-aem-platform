package com.iib.platform.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.iib.platform.services.SolrSearchResultService;
import com.iib.solr.services.SolrServerConfiguration;

/**
 * SOLR Search Servlet
 *
 * @author TechChefz (TCZ Consulting LLP)
 *
 */
@Component(
		immediate = true,
		service = { Servlet.class },
		enabled = true,
		property = {
				Constants.SERVICE_DESCRIPTION + "=Solr Search Servlet",
				"sling.servlet.methods=" + HttpConstants.METHOD_GET,
				"sling.servlet.paths=" + "/bin/solrSearch"
		})
public class SolrSearchServlet extends SlingAllMethodsServlet {

	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(SolrSearchServlet.class);
	
	@Reference
	SolrServerConfiguration solrConfig;
	
	@Reference
	SolrSearchResultService solrSearch;
	
	private String solrProtocol;
	private String solrServerName;
	private String solrServerPort;
	private String solrCoreName;
	
	private String url;
	private String queryParam;
	
	@Activate
	public void activate() {
		log.info("Activated SolrSearchServlet");
		
		this.solrProtocol = solrConfig.getSolrProtocol();
		this.solrServerName = solrConfig.getSolrServerName();
		this.solrServerPort = solrConfig.getSolrServerPort();
		this.solrCoreName = solrConfig.getSolrCoreName();
		
	}
	
	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		
		queryParam = "*:*";
		
		this.url = solrProtocol + "://" + solrServerName + ":" 
				+ solrServerPort + "/solr/" 
				+ solrCoreName + "/select?indent=on&q=" 
				+ queryParam + "&wt=json";
		
		out.println(solrSearch.solrSearchResult(url));
		
	}

}
