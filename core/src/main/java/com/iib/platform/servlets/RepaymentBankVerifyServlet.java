package com.iib.platform.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.json.JSONObject;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.iib.platform.services.EmandateBankDataService;

/**
 * Repayment Bank Verify Servlet 
 *
 * @author TechChefz (TCZ Consulting LLP)
 *
 */
@Component(
		service = { Servlet.class }, 
		name = "Repayment Bank Verify Servlet",
		enabled = true,
		property = {
				Constants.SERVICE_DESCRIPTION + "=" + "Repayment Bank Verify Servlet",
				"sling.servlet.methods=" + HttpConstants.METHOD_POST,
				"sling.servlet.paths="+ "/bin/repaymentBankVerify"
		})
public class RepaymentBankVerifyServlet extends SlingAllMethodsServlet {
	
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(RepaymentBankVerifyServlet.class);
	
	@Reference
	EmandateBankDataService emandateBankDataService;
	
	@Activate
	public void activate() {
		log.info("Activated RepaymentBankVerifyServlet");
	}
	
	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		
		String repaymentBankCode = request.getParameter("repaymentBankCode");
		
		JSONObject jsonObj = new JSONObject();
		jsonObj = emandateBankDataService.getRepaymentBankName(repaymentBankCode);
		out.println(jsonObj);
	
	}

}
