package com.iib.platform.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.json.JSONArray;
import org.json.JSONObject;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.iib.platform.services.SoapAPIService;

/**
 * E-NACH DOB Servlet
 *
 * @author TechChefz (TCZ Consulting LLP)
 *
 */
@Component(
		service = { Servlet.class }, 
		name = "E-NACH DOB Check Servlet",
		enabled = true,
		property = {
				Constants.SERVICE_DESCRIPTION + "=" + "E-NACH DOB Servlet",
				"sling.servlet.methods=" + HttpConstants.METHOD_POST,
				"sling.servlet.paths="+ "/bin/dobVerify"
		})
public class EnachDOBServlet extends SlingAllMethodsServlet {

	private static final long serialVersionUID = 1L;
	
	private static Logger log = LoggerFactory.getLogger(EnachDOBServlet.class);
	
	@Reference
	SoapAPIService soapApiService;
	
	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		String dob = request.getParameter("dob");
		String mobileNumber = request.getParameter("mobile");
	
		try {
			Cookie dobCookie = new Cookie("userDob",dob);
			dobCookie.setMaxAge(60 * 24 * 3600);
			dobCookie.setPath("/");
			response.addCookie(dobCookie);
			
			JSONArray jsonArray = soapApiService.getAccountDetails(mobileNumber);
			String tempDate = "";
			JSONArray accountsFromDOB = new JSONArray();
			
			for(int i=0; i<jsonArray.length(); i++) {
				JSONObject tempJson = jsonArray.getJSONObject(i);
				tempDate = tempJson.getString("DOB");
				if(tempDate.equals(dob)) {
					accountsFromDOB.put(tempJson);
				}
			}
			
			out.println(accountsFromDOB);

		} catch(Exception e) {
			log.info("Exception in e-NACH DOB Servlet" + e.getMessage());
		}
	}
}
