package com.iib.platform.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.json.JSONObject;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.iib.platform.services.config.BillDeskServletConfig;

/**
 * Bill Desk Servlet
 *
 * @author TechChefz (TCZ Consulting LLP)
 *
 */
@Component(
		service = { Servlet.class },
		enabled = true,
		configurationPolicy = ConfigurationPolicy.REQUIRE,
		name = "Bill Desk Servlet", 
		property = {
				Constants.SERVICE_DESCRIPTION + "=" + "BillDesk Servlet",
				"sling.servlet.methods=" + HttpConstants.METHOD_POST,
				"sling.servlet.paths=" + "/bin/billDeskService"
		})
@Designate(ocd = BillDeskServletConfig.class)
public class BillDeskServlet extends SlingAllMethodsServlet {

	private static final Logger log = LoggerFactory.getLogger(BillDeskServlet.class);
	private static final long serialVersionUID = 1L;
	
	private String redirectUrl;
	private String checksumKey;
	private String billDeskurl;
	
	@Activate
	protected void activate(BillDeskServletConfig config) {
		log.info("BillDesk Servlet has been activated");
		
		this.redirectUrl = config.getRedirectionUrl();
		this.checksumKey = config.checkSumKey();
		this.billDeskurl = config.billDeskUrl();
	}

	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		
		String freq = request.getParameter("frequency");
		String ifsc = request.getParameter("ifsc");
		String bankAccountType = request.getParameter("bankAccountType");
		
		try {
			Cookie responseCookie = request.getCookie("userCookie");
			String cookie = responseCookie.getValue();
			JSONObject jsonObj = new JSONObject(cookie);
			
			/*SIDetails token*/
			String accountnumber = jsonObj.getString("Other_bank_account_number");
			String accounttype = bankAccountType;
			String SIamount = jsonObj.getString("Instructed_amt");
			String SIamounttype = "Max";
			int startdate = jsonObj.getInt("EMI_start_date");
			int enddate = jsonObj.getInt("EMI_END_date");
			String frequency = freq;
			String Ref1 = "NA";
			String Ref2 = jsonObj.getString("loanNumber"); 
			String customername = jsonObj.getString("Name");
			String mandaterefno = "NA";
			String Ifsc = ifsc;
			String micr = "NA";
			String aadhaar = "NA";
			String payer2name = "NA";
			String payer3name = "NA";

			/*BillDesk Request Object*/

			String MerchantID = "INDUSBANKB";
			Long CustomerID = (long) Math.floor(Math.random() * 9_000_000_000L) + 1_000_000_000L;
			String SIDetails = null;
			String TxnAmount = "1.00";
			String BankID = "INDUSBANKACH";
			String Filler2 = "NA";
			String Filler3 = "NA";
			String CurrencyType = "INR";
			String ItemCode = "DIRECT";
			String TypeField1 = "R";
			String UserID = "indusbankb";
			String Filler4 = "NA";
			String Filler5 = "NA";
			String TypeField2 = "F";
			String AdditionalInfo1 = jsonObj.getString("loanNumber"); 
			String AdditionalInfo2 = jsonObj.getString("Instructed_amt"); //To be display on RESPONSE Page (Sorry or Thank-you)
			String AdditionalInfo3 = jsonObj.getString("MobileNo"); 
			String AdditionalInfo4 = "NA";
			String AdditionalInfo5 = "NA";
			String AdditionalInfo6 = "NA";
			String AdditionalInfo7 = jsonObj.getString("Bank_code");
			
			/*Request GET URL : https://uat.billdesk.com/billpay/EMandateController?action=EmandateIndusBankReg&msg=*/
			
			//String ChecksumKey = "BgY9RiahkPrG";
			String requestSIDetails = accountnumber + ":" + accounttype + ":" + SIamount + ":" + SIamounttype + ":" + startdate + ":" + enddate + ":" + frequency + ":" + Ref1 + ":" + Ref2 + ":" + customername + ":" + mandaterefno + ":" + Ifsc + ":" + micr + ":" + aadhaar + ":" + payer2name + ":" + payer3name;
			SIDetails = requestSIDetails;   
			String requestDetails = MerchantID + "|" + CustomerID + "|" + SIDetails + "|" + TxnAmount + "|" + BankID + "|" + Filler2 + "|" + Filler3 + "|" + CurrencyType + "|" + ItemCode + "|" + TypeField1 + "|" + UserID + "|" + Filler4 + "|" + Filler5 + "|" + TypeField2 + "|" + AdditionalInfo1 + "|" + AdditionalInfo2 + "|" + AdditionalInfo3 + "|" + AdditionalInfo4 + "|" + AdditionalInfo5 + "|" + AdditionalInfo6 + "|" + AdditionalInfo7 + "|" + redirectUrl;
			String Checksum = HmacSHA256(requestDetails, checksumKey);
			String requestString =  requestDetails +"|"+ Checksum;

			log.info(requestString + " :: Request String");

			String url = billDeskurl+requestString;
			out.println(url);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String HmacSHA256(String message, String secret) {
		try {
			Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
			SecretKeySpec secret_key = new SecretKeySpec(secret.getBytes(), "HmacSHA256");
			sha256_HMAC.init(secret_key);
			byte raw[] = sha256_HMAC.doFinal(message.getBytes());
			StringBuffer ls_sb = new StringBuffer();
			for (int i = 0; i < raw.length; i++)
				ls_sb.append(char2hex(raw[i]));
			return ls_sb.toString();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	private static String char2hex(byte x) {
		char arr[] = {
				'0',
				'1',
				'2',
				'3',
				'4',
				'5',
				'6',
				'7',
				'8',
				'9',
				'A',
				'B',
				'C',
				'D',
				'E',
				'F'
		};

		char c[] = {
				arr[(x & 0xF0) >> 4],
				arr[x & 0x0F]
		};
		return (new String(c));
	}
}