package com.iib.platform.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.json.JSONObject;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.iib.platform.services.EmandateBankDataService;

/**
 * IFSC Code Verify Servlet 
 *
 * @author TechChefz (TCZ Consulting LLP)
 *
 */
@Component(
		service = { Servlet.class }, 
		name = "IFSC Code Verify Servlet",
		enabled = true,
		property = {
				Constants.SERVICE_DESCRIPTION + "=" + "IFSC Code Verify Servlet",
				"sling.servlet.methods=" + HttpConstants.METHOD_POST,
				"sling.servlet.paths="+ "/bin/ifscVerify"
		})
public class IfscCodeVerifyServlet extends SlingAllMethodsServlet {

	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(IfscCodeVerifyServlet.class);
	
	@Reference
	EmandateBankDataService emandateBankDataService;
	
	@Activate
	public void activate() {
		log.info("Activated IfscCodeVerifyServlet");
	}
	
	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		
		String repaymentBankName = request.getParameter("repaymentBankName");
		String ifscCode = request.getParameter("ifsc");
		String requestedBankCode = ifscCode.substring(0, Math.min(ifscCode.length(), 4));
		
		JSONObject jsonObj = new JSONObject();
		jsonObj = emandateBankDataService.getIfscStatus(requestedBankCode, repaymentBankName);
		
		out.println(jsonObj);
		
	}
}
