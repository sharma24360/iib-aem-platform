package com.iib.platform.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.json.JSONObject;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.iib.platform.api.request.RequestObject;
import com.iib.platform.api.response.ResponseBody;
import com.iib.platform.api.response.ResponseObject;
import com.iib.platform.services.HttpAPIService;

@Component(
		service = { Servlet.class }, name = "SOAP Generate OTP Servlet", property = {
		Constants.SERVICE_DESCRIPTION + "=" + "SOAP Generate OTP Servlet",
		"sling.servlet.methods=" + HttpConstants.METHOD_GET,
        "sling.servlet.paths="+ "/bin/soapCallOTP"
})
public class SOAPGenerateOTPServlet extends SlingAllMethodsServlet {
	
	private static final long serialVersionUID = 1L;

	@Reference
	private HttpAPIService httpAPIService;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {

		PrintWriter out = response.getWriter();
		String mobile = request.getParameter("mobile");
	    	try {
	    		
				String requestUrl = "https://ibluatapig.indusind.com/app/uat/IndusInd_WEB_IndusIndwsvoiceCall?client_id=020939a3-017d-40d0-b011-511c2f52631b&client_secret=F8fF4nK5bY4aE3dQ7uW1jY7hP7bI1sY5qW0hD6tJ5kH0iX4oO1";
				RequestObject requestObject = new RequestObject();
				requestObject.setRequestUrl(requestUrl);
				ResponseObject responseObject = httpAPIService.getSOAPResponse(requestObject, mobile);
				ResponseBody body = responseObject.getResponseBody();
				String json = body.getResponseContentXML();
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("status", json);
				out.println(jsonObj);
				out.flush();
		
	    	}
	    	catch(Exception e) {
	    		out.println("Servlet Exception :: " + e.getMessage());
	    	}

		}

}
