package com.iib.platform.common.models;

import java.util.HashMap;
import java.util.List;

import com.iib.platform.common.objects.WCMComponent;

public interface BreadcrumbModel extends BaseComponentModel{

	public List<HashMap<String, String>> getBreadcrumbList();
	
}
