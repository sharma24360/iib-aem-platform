package com.iib.platform.services.impl;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URI;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONObject;
import org.osgi.framework.Constants;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.iib.platform.api.request.RequestObject;
import com.iib.platform.api.response.ResponseBody;
import com.iib.platform.api.response.ResponseObject;
import com.iib.platform.services.HttpAPIService;

/**
 * HTTP API Service Implementation
 *
 * @author TechChefz (TCZ Consulting LLP)
 *
 */
@Component(
		immediate = true,
		service = { HttpAPIService.class },
		enabled = true,
		property = {
				Constants.SERVICE_DESCRIPTION + "=" + "HTTP API Service Implementation"
		}
		)
public class HttpAPIServiceImpl implements HttpAPIService {

	private static final Logger log = LoggerFactory.getLogger(HttpAPIServiceImpl.class);

	RequestObject requestObject;

	ResponseObject responseObject;

	HttpClient client;

	@Activate
	protected final void activate() {
		log.info("Activated HttpAPIServiceImpl");
	}

	@Deactivate
	protected void deactivate(ComponentContext ctx) throws Exception {
		log.info("Deactivated HttpAPIServiceImpl");
	}

	@Override
	public ResponseObject getSMSResponse(RequestObject requestObject, String mobileNumber, int otp) {

		responseObject = new ResponseObject();
		try {
			HttpClient httpclient = HttpClientBuilder.create().build();
			URI uri = new URIBuilder(requestObject.getRequestUrl()).addParameter("feedid", "369236").addParameter("username", "9899088737").addParameter("password", "tgmtg").addParameter("To", mobileNumber).addParameter("text", otp + " is your One Time Password for Request 543534 placed with IndusInd Bank. Please do not share this OTP with anyone for security reasons.").build();
			HttpGet httpGetRequest = new HttpGet(uri);

			HttpResponse resp = (HttpResponse) httpclient.execute(httpGetRequest);
			HttpEntity entity = resp.getEntity();

			//Read the content stream
			InputStream instream = entity.getContent();
			BufferedReader br =  new BufferedReader(new InputStreamReader((instream)));

			StringBuilder sb = new StringBuilder();
			String line;
			while ((line = br.readLine()) != null) {
				sb.append(line);
				sb.append("\n");
			}

			//Extracting TID from XML Document 
			String xmlData = sb.toString();
			String tid = "";
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document document = builder.parse(new InputSource(new StringReader(xmlData)));
			NodeList nodeList = document.getElementsByTagName("MID");
			for(int i=0; i<nodeList.getLength(); i++) {
				Node eachNode = nodeList.item(i);
				Element element = (Element) eachNode;
				tid = element.getAttribute("TID");
			}

			ResponseBody responseBody = new ResponseBody();
			responseBody.setResponseContentXML(tid);
			responseObject.setResponseBody(responseBody);
		}
		catch(Exception e) {
			log.info(e.getMessage());
		}

		return responseObject;
	}

	@Override
	public ResponseObject getSOAPResponse(RequestObject requestObject, String mobileNumber) {
		responseObject = new ResponseObject();
		try {
			final String PREFERRED_PREFIX = "soapenv";

			MessageFactory messageFactory = MessageFactory.newInstance();
			SOAPMessage soapMessage = messageFactory.createMessage();
			String uri = "http://MDESBDEVPRD.IBL.COM/IndusInd_WEB_IndusInd.ws:voiceCall";
			SOAPEnvelope envelope = soapMessage.getSOAPPart().getEnvelope();
			SOAPHeader soapHeader = soapMessage.getSOAPHeader();
			envelope.removeNamespaceDeclaration(envelope.getPrefix());
			envelope.setPrefix(PREFERRED_PREFIX);
			envelope.addNamespaceDeclaration("ind", uri);
			soapHeader.setPrefix(PREFERRED_PREFIX);
			SOAPBody soapBody = soapMessage.getSOAPBody();
			soapBody.setPrefix(PREFERRED_PREFIX);

			SOAPBodyElement soapBodyElement = soapBody.addBodyElement(envelope.createQName("voiceCall","ind"));
			SOAPElement voiceCallNode = soapBodyElement.addChildElement(envelope.createName("voiceCallReq"));
			SOAPElement msidn = voiceCallNode.addChildElement(envelope.createName("MSISDN"));
			msidn.addTextNode(mobileNumber);
			SOAPElement otpNode = voiceCallNode.addChildElement(envelope.createName("OTP"));
			otpNode.addTextNode("123456");
			SOAPElement refNode = voiceCallNode.addChildElement(envelope.createName("REF"));
			refNode.addTextNode("654321");
			SOAPElement sourceNode = voiceCallNode.addChildElement(envelope.createName("SourceSys"));
			sourceNode.addTextNode("OAO");
			soapMessage.saveChanges();

			SOAPConnectionFactory soapConFactory = SOAPConnectionFactory.newInstance();
			SOAPConnection connection = soapConFactory.createConnection();
			URL url = new URL(requestObject.getRequestUrl());
			HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
			con.connect();
			SOAPMessage soapResponse = connection.call(soapMessage,url);

			String soapResp = "";
			ByteArrayOutputStream responseXML = new ByteArrayOutputStream();
			soapResponse.writeTo(responseXML);
			soapResp = responseXML.toString();

			String status="";
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = factory.newDocumentBuilder();
			Document document = documentBuilder.parse(new InputSource(new StringReader(soapResp)));
			NodeList nodeList = document.getElementsByTagName("voiceCallResp");
			for(int i=0; i<nodeList.getLength(); i++) {
				Node node = nodeList.item(i);
				if(node.getNodeType() == Node.ELEMENT_NODE) {
					Element element = (Element) node;
					NodeList childNode = element.getChildNodes();
					for(int j=0; j<childNode.getLength(); j++) {
						Node eachChild = childNode.item(j);
						Element eachElement = (Element) eachChild;
						if(eachElement.getTagName() == "status") {
							status = eachElement.getTextContent();
						}
					}
				}
			}

			ResponseBody responseBody = new ResponseBody();
			responseBody.setResponseContentXML(status);
			responseObject.setResponseBody(responseBody);
		}

		catch(Exception e) {
			log.info("Exception Message for HttpAPIServiceIMPL :: "  + e.getMessage());
		}

		return responseObject;
	}

	@Override
	public JSONObject getviaAccountNo(String mobileNumber) {
		JSONObject jsonObj = new JSONObject();
		try {
			HttpClient httpclient = HttpClientBuilder.create().build();
			URI uri = new URIBuilder("https://ibluatapig.indusind.com/app/uat/emandateFlux/status?id=12345&mobileNo=" + mobileNumber +"&accountNo=706000016705").build();
			HttpGet httpGetRequest = new HttpGet(uri);
			httpGetRequest.setHeader("X-IBM-Client-Secret","F8fF4nK5bY4aE3dQ7uW1jY7hP7bI1sY5qW0hD6tJ5kH0iX4oO1");
			httpGetRequest.setHeader("X-IBM-Client-Id","020939a3-017d-40d0-b011-511c2f52631b");
			HttpResponse resp = (HttpResponse) httpclient.execute(httpGetRequest);
			HttpEntity entity = resp.getEntity();

			//Read the content stream
			InputStream instream = entity.getContent();
			BufferedReader br =  new BufferedReader(new InputStreamReader((instream)));
			StringBuilder sb = new StringBuilder();
			String line;
			while ((line = br.readLine()) != null) {
				sb.append(line);
				sb.append("\n");
			}

			jsonObj.put("accounts", new JSONObject(sb.toString()));

		}
		catch(Exception e) {
			log.info("Exception in getViaAccountNo :: " + e.getMessage());
		}
		return jsonObj;
	}

	@Override
	public JSONObject getAccountViaDOB() {
		JSONObject jsonObj = new JSONObject();
		try {
			HttpClient httpclient = HttpClientBuilder.create().build();
			URI uri = new URIBuilder("https://ibluatapig.indusind.com/app/uat/emandateFlux/status?id=12345&mobileNo=9448712684&accountNo=706000016705").build();
			HttpGet httpGetRequest = new HttpGet(uri);
			httpGetRequest.setHeader("X-IBM-Client-Secret","F8fF4nK5bY4aE3dQ7uW1jY7hP7bI1sY5qW0hD6tJ5kH0iX4oO1");
			httpGetRequest.setHeader("X-IBM-Client-Id","020939a3-017d-40d0-b011-511c2f52631b");
			HttpResponse resp = (HttpResponse) httpclient.execute(httpGetRequest);
			HttpEntity entity = resp.getEntity();

			//Read the content stream
			InputStream instream = entity.getContent();
			BufferedReader br =  new BufferedReader(new InputStreamReader((instream)));
			StringBuilder sb = new StringBuilder();
			String line;
			while ((line = br.readLine()) != null) {
				sb.append(line);
				sb.append("\n");
			}

			jsonObj.put("accounts", new JSONObject(sb.toString()));

		}
		catch(Exception e) {
			log.info("Exception in getAccountsViaDOB :: " + e.getMessage());
		}
		return jsonObj;
	}

	@Override
	public ResponseObject callAPI(RequestObject requestObject) {
		return null;
	}

}