package com.iib.platform.services;

import org.json.JSONArray;
import org.osgi.annotation.versioning.ConsumerType;

@ConsumerType
public interface SolrSearchResultService {
	
	public JSONArray solrSearchResult(String url);

}
