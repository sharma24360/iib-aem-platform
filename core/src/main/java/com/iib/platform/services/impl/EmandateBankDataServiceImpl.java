package com.iib.platform.services.impl;

import org.json.JSONArray;
import org.json.JSONObject;
import org.osgi.framework.Constants;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.iib.platform.services.EmandateBankDataService;
import com.iib.platform.services.config.EmandateBankDataServiceConfig;

/**
 * E-Mandate Bank Data Service Implementation
 *
 * @author TechChefz (TCZ Consulting LLP)
 *
 */
@Component(
		immediate = true,
		service = { EmandateBankDataService.class },
		enabled = true,
		property = {
				Constants.SERVICE_DESCRIPTION + "=E-Mandate Bank Data Service Implementation"
		}
	)
@Designate(ocd=EmandateBankDataServiceConfig.class)
public class EmandateBankDataServiceImpl implements EmandateBankDataService {

	/*Logger*/
	private static Logger log = LoggerFactory.getLogger(EmandateBankDataServiceImpl.class);
	
	String[] bankData = {};
	String[] frequencyBankData = {};
	String[] tempBankData = {};
	
	@Activate()
	public void activate(EmandateBankDataServiceConfig config) {
		log.info("Activated EmandateBankDataServiceImpl");
		
		bankData = config.getBankCode();
		frequencyBankData = config.getFrequencyBankData();
	}
	
	@Override
	public JSONObject getIfscStatus(String ifsc, String repaymentBankName) {
		JSONObject jsonObj = new JSONObject();
		
		try {
			for(int i=0; i<bankData.length; i++) {
				tempBankData = bankData[i].split(":");
				if((repaymentBankName.equalsIgnoreCase(tempBankData[0])) && (ifsc.equalsIgnoreCase(tempBankData[1]))) {
					jsonObj.put("success", "true");
					break;
				} else {
					jsonObj.put("success", "false");
				}				
			}
		} catch(Exception e) {
			log.error("Exception in getIfscCode Method :: " + e.getMessage());
		}
		
		return jsonObj;
	}
	
	
	@Override
	public JSONObject getRepaymentBankName(String repaymentBankCode) {
		JSONObject res = new JSONObject();
		
		try {
			for(int i=0; i<bankData.length; i++) {
				tempBankData = bankData[i].split(":");
				if(repaymentBankCode.equalsIgnoreCase(tempBankData[1])) {
					res.put("success", "true");
					res.put("bankName", tempBankData[0]);
					for(int j=0; j<frequencyBankData.length; j++) {
						if(frequencyBankData[j].equalsIgnoreCase(tempBankData[0])) {
							res.put("frequencyType","MNTH");
							break;
						} else {
							res.put("frequencyType", "ADHO");
						}
					}
					
					break;
				} else {
					res.put("success", "false");
				}				
			}
		} catch(Exception e) {
			log.error("Exception in getRepaymentBankName Method :: " + e.getMessage());
		}		
		return res;
	}
	
	@Override
	public Boolean getRepaymentBankCodeVerify(String bankCode) {
		Boolean status = false;
		
		try {
			for(int i=0; i<bankData.length; i++) {
				tempBankData = bankData[i].split(":");
				if(bankCode.equalsIgnoreCase(tempBankData[1])) {
					status = true;
					break;
				} else {
					status = false;
				}				
			}
		} catch(Exception e) {
			log.error("Exception in getRepaymentBankCodeVerify Method :: " + e.getMessage());
		}		
		return status;
	}
	
	@Override
	public JSONArray getBankList() {
		JSONArray bankList = new JSONArray();
		try {
			int i;
			
			for(i=0; i<bankData.length; i++) {
				tempBankData = bankData[i].split(":");
				bankList.put(tempBankData[0]);
			}
						
		} catch(Exception e) {
			log.error("Exception in getBankList Method :: " + e.getMessage());
		}
		
		return bankList;
	}
	
	
	@Deactivate
    protected void deactivate(ComponentContext ctx) throws Exception {
        log.info("Deactivated EmandateBankDataServiceImpl");
    }
}
