package com.iib.platform.services;

import org.osgi.annotation.versioning.ConsumerType;

@ConsumerType 
public interface StoreOTPService {
	
	public void storeOTP(String mobile, int otp);

}
