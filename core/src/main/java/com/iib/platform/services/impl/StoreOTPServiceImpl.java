package com.iib.platform.services.impl;

import java.util.HashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.Session;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.framework.Constants;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.iib.platform.services.StoreOTPService;

/**
 * Store OTP Service Implementation
 *
 * @author TechChefz (TCZ Consulting LLP)
 *
 */
@Component(
		immediate = true,
		service = { StoreOTPService.class },
		configurationPid = "com.iib.platform.services.impl.StoreOTPServiceImpl",
		property = {
				Constants.SERVICE_DESCRIPTION + "=" + "Store OTP Service Implementation"
		})
public class StoreOTPServiceImpl implements StoreOTPService {

	/**Static Logger*/
	private static Logger log = LoggerFactory.getLogger(StoreOTPServiceImpl.class);

	@Reference
	private ResourceResolverFactory resourceResolverFactory;

	ResourceResolver resourceResolver;

	Session session;

	@Activate
	protected void activate() {
		log.info("Activated StoreOTPServiceImpl");

		Map<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put(ResourceResolverFactory.SUBSERVICE, "replicationService");
		try {
			resourceResolver = resourceResolverFactory.getServiceResourceResolver(paramMap);
			session = resourceResolver.adaptTo(Session.class);
			log.info("StoreOTP Service UserID: " + session.getUserID());
		} catch (LoginException e) {
			log.error("Login Exception in StoreOTP Service :: " + e.getMessage());
		}
	}

	@Deactivate
	protected void deactivate(ComponentContext ctx) throws Exception {
		log.info("Deactivated StoreOTPServiceImpl");
		session.logout();
	}

	@Override
	public void storeOTP(String mobile, int otp) {

		/**Code Snippet to Store OTP in "otpUsers" Node*/
		try {
			Node rootNode = session.getRootNode();
			Node apps = rootNode.getNode("apps");

			if(!apps.hasNode("otpUsers")) {
				Node otpUsers = apps.addNode("otpUsers", "nt:unstructured");
				Node currentNode = otpUsers.addNode(mobile,"nt:unstructured");
				Node currentUserNode = otpUsers.getNode(mobile);
				currentUserNode.setProperty("otp", otp);
			} else {
				Node otpUsers = apps.getNode("otpUsers");
				if(!otpUsers.hasNode(mobile)) {
					Node currentUserNode = otpUsers.addNode(mobile, "nt:unstructured");
					if(currentUserNode.hasProperty("otp")) {
						currentUserNode.getProperty("otp").remove();
						resourceResolver.commit();

						currentUserNode.setProperty("otp", otp);
						resourceResolver.commit();
					} else {
						currentUserNode.setProperty("otp", otp);
					}
				} else {
					Node currentUserNode = otpUsers.getNode(mobile);
					if(currentUserNode.hasProperty("otp")) {
						currentUserNode.getProperty("otp").remove();
						resourceResolver.commit();

						currentUserNode.setProperty("otp", otp);
						resourceResolver.commit();
					} else {
						currentUserNode.setProperty("otp", otp);
					}
				}
			}
			resourceResolver.commit();
		} catch(Exception e) {
			log.error("Exception in StoreOTPService :: " + e.getMessage());
		}
	}

}
