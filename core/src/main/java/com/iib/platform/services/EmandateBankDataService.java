package com.iib.platform.services;

import org.json.JSONArray;
import org.json.JSONObject;
import org.osgi.annotation.versioning.ConsumerType;

@ConsumerType
public interface EmandateBankDataService {
	
	public JSONObject getIfscStatus(String ifsc, String repaymentBankName);
	
	public JSONObject getRepaymentBankName(String repaymentBankCode);
	
	public Boolean getRepaymentBankCodeVerify(String bankCode);
	
	public JSONArray getBankList();

}
