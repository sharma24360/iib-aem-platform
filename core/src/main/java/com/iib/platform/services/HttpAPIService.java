package com.iib.platform.services;

import org.json.JSONObject;

import com.iib.platform.api.request.RequestObject;
import com.iib.platform.api.response.ResponseObject;

/**
 * HTTP API Service
 *
 * @author TechChefz (TCZ Consulting LLP)
 *
 */

public interface HttpAPIService {
	
	public ResponseObject callAPI(RequestObject requestObject);
	
	public ResponseObject getSMSResponse(RequestObject requestObject, String mobileNumber, int otp);
	
	public ResponseObject getSOAPResponse(RequestObject request, String mobileNumber);
	
	public JSONObject getviaAccountNo(String mobileNumber);
	
	public JSONObject getAccountViaDOB();

}
