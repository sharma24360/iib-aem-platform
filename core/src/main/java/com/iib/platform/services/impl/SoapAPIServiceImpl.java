package com.iib.platform.services.impl;

import java.io.ByteArrayOutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.CDATASection;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.InputSource;

import com.iib.platform.api.request.RequestObject;
import com.iib.platform.api.response.ResponseObject;
import com.iib.platform.services.SoapAPIService;
import com.iib.platform.services.config.SoapAPIServiceConfig;

/**
 * SOAP API Service Implementation 
 *
 * @author TechChefz (TCZ Consulting LLP)
 *
 */
@Component(
		immediate = true,
		service = { SoapAPIService.class },
		enabled = true,
		property = {
				Constants.SERVICE_DESCRIPTION + "=" + "SOAP API Service"
	})
@Designate(ocd=SoapAPIServiceConfig.class)
public class SoapAPIServiceImpl implements SoapAPIService {
	
	private static Logger log = LoggerFactory.getLogger(SoapAPIServiceImpl.class);
	
	ResponseObject responseObject;
	RequestObject requestObject;
	
	private String clientSecretKey;
	private String clientSecretValue;
	private String clientIdKey;
	private String clientIdValue;
	
	@Activate
	protected void activate(SoapAPIServiceConfig config) {
		log.info("SOAP API Service has been activated");
		
		clientSecretKey = config.getClientSecretKey();
		clientSecretValue = config.getClientSecretValue();
		clientIdKey = config.getClientID();
		clientIdValue = config.getClientIDValue();
	}
	
	@Override
	public ResponseObject getHelpMeStatus(RequestObject requestObject, String customerName, String mobileNumber, String emailId) {
		responseObject = new ResponseObject();
		
		/*try {	
			final String PREFERRED_PREFIX = "soap";
			
			MessageFactory messageFactory = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL);
			SOAPMessage soapMessage = messageFactory.createMessage();
			String prefix = "tem";
			String uri = "http://tempuri.org/";
			SOAPEnvelope envelope = soapMessage.getSOAPPart().getEnvelope();
			SOAPHeader soapHeader = soapMessage.getSOAPHeader();
			envelope.removeNamespaceDeclaration(envelope.getPrefix());
			envelope.setPrefix(PREFERRED_PREFIX);
			envelope.addNamespaceDeclaration(prefix, uri);
			soapHeader.setPrefix(PREFERRED_PREFIX);
			SOAPBody soapBody = soapMessage.getSOAPBody();
			soapBody.setPrefix(PREFERRED_PREFIX);
		
			DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = documentFactory.newDocumentBuilder();
			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("Talisma");
			
			Element talismaPropertiesDetails = doc.createElement("TalismaPropertiesDetails");
			rootElement.appendChild(talismaPropertiesDetails);
			
			Text custNameNode = doc.createTextNode(customerName);
			Element property1 = doc.createElement("Property");
			String idKey1 = "ID";
			String idValue1 = "27287";
			property1.setAttribute(idKey1, idValue1);
			property1.appendChild(custNameNode);
			talismaPropertiesDetails.appendChild(property1);
			
			Text mobileNumberNode = doc.createTextNode(mobileNumber);
			Element property2 = doc.createElement("Property");
			String idKey2 = "ID";
			String idKeyValue2 = "27291";
			property2.setAttribute(idKey2, idKeyValue2);
			property2.appendChild(mobileNumberNode);
			talismaPropertiesDetails.appendChild(property2);
			
			Text emailIdNode = doc.createTextNode(emailId);
			Element property3 = doc.createElement("Property");
			String idKey3 = "ID";
			String idKeyValue3 = "27289";
			property3.setAttribute(idKey3, idKeyValue3);
			property3.appendChild(emailIdNode);
			talismaPropertiesDetails.appendChild(property3);
			
			doc.appendChild(rootElement);
			
			DOMSource domSource = new DOMSource(doc);
			StringWriter writer = new StringWriter();
			StreamResult result = new StreamResult(writer);
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.transform(domSource, result);
			String innerXml = writer.toString();
			
			SOAPBodyElement createServiceRequest = soapBody.addBodyElement(envelope.createQName("CreateServiceRequest", prefix));
			SOAPElement accountNo = createServiceRequest.addChildElement(envelope.createQName("AccountNo", prefix));
			accountNo.addTextNode("9999inqry1001");
			
			SOAPElement cif_id = createServiceRequest.addChildElement(envelope.createQName("CIF_ID", prefix));
			cif_id.addTextNode("INQRY1");
			
			SOAPElement callRelatedTo = createServiceRequest.addChildElement(envelope.createQName("CallRelatedTo", prefix));
			callRelatedTo.addTextNode("Request_IndusSmart");
			
			SOAPElement callType = createServiceRequest.addChildElement(envelope.createQName("CallType", prefix));
			callType.addTextNode("Customer_Meeting_Feedback");
			
			SOAPElement callSubType = createServiceRequest.addChildElement(envelope.createQName("CallSubType", prefix));
			callSubType.addTextNode("IndusSmart_Feedback");
			
			SOAPElement media = createServiceRequest.addChildElement(envelope.createQName("Media", prefix));
			media.addTextNode("Website");
			
			SOAPElement interactionState = createServiceRequest.addChildElement(envelope.createQName("InteractionState", prefix));
			interactionState.addTextNode("Resolved");
			
			SOAPElement team = createServiceRequest.addChildElement(envelope.createQName("Team", prefix));
			team.addTextNode("Others");
			
			SOAPElement subject = createServiceRequest.addChildElement(envelope.createQName("Subject", prefix));
			subject.addTextNode("Website Compliments");
			
			SOAPElement message = createServiceRequest.addChildElement(envelope.createQName("Message", prefix));
			message.addTextNode("Website Compliments");
			
			SOAPElement sms_flag = createServiceRequest.addChildElement(envelope.createQName("SMS_Flag", prefix));
			sms_flag.addTextNode("false");
			
			SOAPElement email_flag = createServiceRequest.addChildElement(envelope.createQName("Email_Flag", prefix));
			email_flag.addTextNode("false");
			
			SOAPElement xml_str = createServiceRequest.addChildElement(envelope.createQName("XML_Str", prefix));
			CDATASection cdata = xml_str.getOwnerDocument().createCDATASection(innerXml);
			xml_str.appendChild(cdata);
			
			if(StringUtils.isNotBlank(clientIdValue)) {
				log.info("Setting Headers");
				soapMessage.getMimeHeaders().addHeader(clientSecretKey, clientSecretValue);
				soapMessage.getMimeHeaders().addHeader(clientIdKey, clientIdValue);
			} else {
				log.info("Blank Headers");
			}
			soapMessage.saveChanges();
			
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory.createConnection();
			URL url = new URL(requestObject.getRequestUrl());
			SOAPMessage soapResponse = soapConnection.call(soapMessage, url);
			
			String responseXml = "";
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			soapResponse.writeTo(stream);
			responseXml = stream.toString();
			//log.info(responseXml + " :: Help Me XML");
			
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = factory.newDocumentBuilder();
			Document document = documentBuilder.parse(new InputSource(new StringReader(responseXml)));
			
			Map<String, String> resultParam = new HashMap<>();
			NodeList nodeList = document.getElementsByTagName("CreateServiceRequestResponse");
			for(int i=0; i<nodeList.getLength(); i++) {
				Node node = nodeList.item(i);
				if(node.getNodeType() == Node.ELEMENT_NODE) {
					Element element = (Element) node;
					NodeList childNode = element.getChildNodes();
					for(int j=0; j<childNode.getLength(); j++) {
						Node eachChild = childNode.item(j);
						Element eachElement = (Element) eachChild;
						if(eachElement.getTagName() == "CreateServiceRequestResult") {
							resultParam.put("status", eachElement.getTextContent());
						}
						else if(eachElement.getTagName() == "InteractionID") {
							resultParam.put("interactionId",eachElement.getTextContent());
						}
					}
				}
			}
			ResponseBody responseBody = new ResponseBody();
			responseBody.setResponseContentParameterMap(resultParam);
			responseObject.setResponseBody(responseBody);
		}
		catch(Exception e) {
			log.info("Exception in HelpMe Servlet:: " + e.getMessage());
		}*/
		
		return null;
	}
	
	
	@Override
	public JSONArray getAccountDetails(String mobileNumber) {
		
		JSONArray jsonArray= new JSONArray();
		JSONObject obj;
		responseObject = new ResponseObject();
		String endPoint = "https://ibluatapig.indusind.com/app/uat/FIUsbWebServiceService";
		
		try {
			final String PREFERRED_PREFIX = "soapenv";
			
			MessageFactory messageFactory = MessageFactory.newInstance();
			SOAPMessage soapMessage = messageFactory.createMessage();

			String prefix = "web";
			String uri = "http://webservice.fiusb.ci.infosys.com";
			SOAPEnvelope envelope = soapMessage.getSOAPPart().getEnvelope();
			SOAPHeader soapHeader = envelope.getHeader();
			envelope.removeNamespaceDeclaration(envelope.getPrefix());
			envelope.setPrefix(PREFERRED_PREFIX);
			envelope.addNamespaceDeclaration(prefix, uri);
			soapHeader.setPrefix(PREFERRED_PREFIX);
			SOAPBody soapBody = soapMessage.getSOAPBody();
			soapBody.setPrefix(PREFERRED_PREFIX);

			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
			Document doc = docBuilder.newDocument();
			
			Element rootElement = doc.createElement("FIXML");

			String qualifiedName = "xmlns";
			String nameSpaceUri = "http://www.finacle.com/fixml";
			String qualifiedNameSecond = "xmlns:xsi";
			String nameSpaceUriSecond = "http://www.w3.org/2001/XMLSchema-instance";
			String qualifiedNameThird = "xsi:schemaLocation";
			String nameSpaceUriThird = "http://www.finacle.com/fixml executeFinacleScript.xsd";

			/*Add Name space for FIXML rootElement*/
			rootElement.setAttribute(qualifiedName, nameSpaceUri);
			rootElement.setAttribute(qualifiedNameSecond, nameSpaceUriSecond);
			rootElement.setAttribute(qualifiedNameThird, nameSpaceUriThird);

			Element header = doc.createElement("Header");
			rootElement.appendChild(header);

			Element requestHeader = doc.createElement("RequestHeader");
			header.appendChild(requestHeader);

			Element messageKey = doc.createElement("MessageKey");
			requestHeader.appendChild(messageKey);

			long randomNumber = (long) Math.floor(Math.random() * 9000000000000L) + 1000000000000L;
			Text girNumber = doc.createTextNode("Gir_" + randomNumber);

			Element reqUUId = doc.createElement("RequestUUID");
			messageKey.appendChild(reqUUId);
			reqUUId.appendChild(girNumber);

			Element serviceReq = doc.createElement("ServiceRequestId");
			Text serviceReqText = doc.createTextNode("executeFinacleScript");
			serviceReq.appendChild(serviceReqText);
			messageKey.appendChild(serviceReq);

			Element serviceReqVersion = doc.createElement("ServiceRequestVersion");
			Text serviceReqVersionText = doc.createTextNode("10.2");
			serviceReqVersion.appendChild(serviceReqVersionText);
			messageKey.appendChild(serviceReqVersion);

			Element channelId = doc.createElement("ChannelId");
			Text channelIdText = doc.createTextNode("COR");
			channelId.appendChild(channelIdText);
			messageKey.appendChild(channelId);

			Element languageId = doc.createElement("LanguageId");
			messageKey.appendChild(languageId);

			Element reqMessageInfo = doc.createElement("RequestMessageInfo");
			requestHeader.appendChild(reqMessageInfo);

			Element bankId = doc.createElement("BankId");
			reqMessageInfo.appendChild(bankId);

			Element timeZone = doc.createElement("TimeZone");
			reqMessageInfo.appendChild(timeZone);

			Element entityId = doc.createElement("EntityId");
			reqMessageInfo.appendChild(entityId);

			Element entityType = doc.createElement("EntityType");
			reqMessageInfo.appendChild(entityType);

			Element armCorrelationId = doc.createElement("ArmCorrelationId");
			reqMessageInfo.appendChild(armCorrelationId);

			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.ENGLISH);
			Date date = new Date();

			Element messageDateTime = doc.createElement("MessageDateTime");
			Text messageDateTimeText = doc.createTextNode(dateFormat.format(date));
			messageDateTime.appendChild(messageDateTimeText);
			reqMessageInfo.appendChild(messageDateTime);

			Element security = doc.createElement("Security");
			requestHeader.appendChild(security);

			Element token = doc.createElement("Token");
			security.appendChild(token);

			Element passwordToken = doc.createElement("PasswordToken");
			token.appendChild(passwordToken);

			Element userId = doc.createElement("UserId");
			passwordToken.appendChild(userId);

			Element password = doc.createElement("Password");
			passwordToken.appendChild(password);

			Element fICertToken = doc.createElement("FICertToken");
			security.appendChild(fICertToken);

			Element realUserLoginSessionId = doc.createElement("RealUserLoginSessionId");
			security.appendChild(realUserLoginSessionId);

			Element realUser = doc.createElement("RealUser");
			security.appendChild(realUser);

			Element realUserPwd = doc.createElement("RealUserPwd");
			security.appendChild(realUserPwd);

			Element sSOTransferToken = doc.createElement("SSOTransferToken");
			security.appendChild(sSOTransferToken);

			/*SoapBody Starts*/
			Element body = doc.createElement("Body");
			rootElement.appendChild(body);

			Element executeFinacleScriptRequest = doc.createElement("executeFinacleScriptRequest");
			body.appendChild(executeFinacleScriptRequest);

			Element executeFinacleScriptInputVO = doc.createElement("ExecuteFinacleScriptInputVO");
			executeFinacleScriptRequest.appendChild(executeFinacleScriptInputVO);

			Element executeFinacleScriptInputVOReqId = doc.createElement("requestId");
			Text executeFinacleScriptInputVOReqIdText = doc.createTextNode("IBL0662MOBACC.scr");
			executeFinacleScriptInputVOReqId.appendChild(executeFinacleScriptInputVOReqIdText);
			executeFinacleScriptInputVO.appendChild(executeFinacleScriptInputVOReqId);

			Element executeFinacleScript_CustomData = doc.createElement("executeFinacleScript_CustomData");
			executeFinacleScriptRequest.appendChild(executeFinacleScript_CustomData);

			Element mobNumber = doc.createElement("MOB_Number");
			Text mobileText = doc.createTextNode(mobileNumber);
			mobNumber.appendChild(mobileText);
			executeFinacleScript_CustomData.appendChild(mobNumber);

			Element dateOfBirth = doc.createElement("DOB");
			/*Text dobText = doc.createTextNode("");
	        	dateOfBirth.appendChild(dobText);*/
			executeFinacleScript_CustomData.appendChild(dateOfBirth);

			doc.appendChild(rootElement);

			/*Converting XML to String*/
			DOMSource domSource = new DOMSource(doc);
			StringWriter writer = new StringWriter();
			StreamResult result = new StreamResult(writer);
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			transformer.transform(domSource, result);
			String characterData = writer.toString();

			SOAPBodyElement soapBodyElement = soapBody.addBodyElement(envelope.createQName("executeService","web"));
			SOAPElement arg0 = soapBodyElement.addChildElement("arg_0_0");
			CDATASection cdata = arg0.getOwnerDocument().createCDATASection(characterData);
			arg0.appendChild(cdata);

			if(StringUtils.isNotBlank(clientSecretKey)) {
				soapMessage.getMimeHeaders().addHeader(clientSecretKey, clientSecretValue);
				soapMessage.getMimeHeaders().addHeader(clientIdKey, clientIdValue);
			} else {
				log.info("Blank Headers");
			}
			soapMessage.saveChanges();

			/**Print Request*/
			String requestXmlString = "";
			ByteArrayOutputStream reqXml = new ByteArrayOutputStream();
			soapMessage.writeTo(reqXml);
			requestXmlString = reqXml.toString();
			Document reqDoc = convertStringToDocument(requestXmlString);
			String xmlOutput = convertDocumentToString(reqDoc,"2");
			log.info(xmlOutput + " :: SOAP API Request (Account Details)");

			/*Creating Connection*/
			SOAPConnectionFactory soapConFactory = SOAPConnectionFactory.newInstance();
			SOAPConnection connection = soapConFactory.createConnection();
			URL url = new URL(endPoint);
			SOAPMessage soapResponse = connection.call(soapMessage,url);
			
			// Print Response
			String responseXML = "";
			ByteArrayOutputStream resXml = new ByteArrayOutputStream();
			soapResponse.writeTo(resXml);
			responseXML = resXml.toString();
			Document respDoc = convertStringToDocument(responseXML);
			String finalOutpt = convertDocumentToString(respDoc,"2");
			log.info(finalOutpt + " :: SOAP API Response (Account Details)");
			
			/*Parsing SOAPBody*/
			SOAPBody responseBody = soapResponse.getSOAPBody();
			NodeList executeNodeList = responseBody.getElementsByTagName("executeServiceReturn");
			Element executeElement = (Element) executeNodeList.item(0);
			Node child = executeElement.getFirstChild();
			String charData = "";
			
			
			if(child instanceof CharacterData) {
				charData = ((CharacterData) child).getData();
			}
			
			Document responseDoc = convertStringToDocument(charData);
			NodeList accountDetailList = responseDoc.getElementsByTagName("AccountInquiry");

			for(int index=0; index < accountDetailList.getLength(); index++) {
				Node node = accountDetailList.item(index);
				if(node.getNodeType() == Node.ELEMENT_NODE) {
					Element element = (Element) node;
					NodeList childNodes = element.getChildNodes();
					for(int j=0; j<childNodes.getLength(); j++) {
						obj = new JSONObject();
						Node eachChild = childNodes.item(j);
						NodeList childrensOfDead = eachChild.getChildNodes();
						for(int k=0; k < childrensOfDead.getLength(); k++) {
							Node eachChildren = childrensOfDead.item(k);
							obj.put(eachChildren.getNodeName(), eachChildren.getTextContent());
						}
						jsonArray.put(obj);
					}
				}
			}
		}
		catch(Exception e) {
			log.info("Exception in AccountDetails :: " + e.getMessage());
		}
		return jsonArray;
	}
	
	private static Document convertStringToDocument(String xmlStr) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();  
		DocumentBuilder builder;  
		try {  
			builder = factory.newDocumentBuilder();  
			Document doc = builder.parse(new InputSource(new StringReader(xmlStr))); 
			return doc;
		} catch (Exception e) {  
			e.printStackTrace();  
		} 
		return null;
	}
	
	private static String convertDocumentToString(Document doc, String indent) {
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer;
		try {
			transformer = tf.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", indent);
			StringWriter writer = new StringWriter();
			transformer.transform(new DOMSource(doc), new StreamResult(writer));
			String output = writer.getBuffer().toString();
			return output.replaceAll("&lt;","<").replaceAll("&gt;", ">");
		} catch (TransformerException e) {
			e.printStackTrace();
		}
		return null;
	}
}