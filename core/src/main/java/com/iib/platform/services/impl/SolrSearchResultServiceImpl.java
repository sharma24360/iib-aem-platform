package com.iib.platform.services.impl;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONObject;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.iib.platform.services.SolrSearchResultService;

@Component(
		immediate = true,
		service = { SolrSearchResultService.class },
		enabled = true,
		property = {
				Constants.SERVICE_DESCRIPTION + "=IndusInd Solr Search Result Service Config"
		})
public class SolrSearchResultServiceImpl implements SolrSearchResultService {

	/**Static Logger*/
	private static Logger log = LoggerFactory.getLogger(SolrSearchResultServiceImpl.class);

	@Activate
	protected void activate() {
		log.info("SolrSearchResultServiceImpl Activated!");
	}

	
	@Override
	public JSONArray solrSearchResult(String url) {
		JSONArray res = null;
		try {
			HttpClient httpclient = HttpClientBuilder.create().build();
			URI uri = new URIBuilder(url).build();
			HttpGet httpGetRequest = new HttpGet(uri);
			HttpResponse resp = (HttpResponse) httpclient.execute(httpGetRequest);
			HttpEntity entity = resp.getEntity();

			//Read the content stream
			InputStream instream = entity.getContent();
			BufferedReader br = new BufferedReader(new InputStreamReader((instream)));
			StringBuilder sb = new StringBuilder();
			String line;
			while ((line = br.readLine()) != null) {
				sb.append(line);
				sb.append("\n");
			}

			JSONObject output = new JSONObject(sb.toString());
			res = new JSONArray(output.getJSONObject("response").getJSONArray("docs").toString());
		}
		catch(Exception e) {
			log.info("Exception in Solr Search :: " + e.getMessage());
		}
		return res;
	}

}
