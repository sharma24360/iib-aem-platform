package com.iib.platform.enach.models.impl;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.iib.platform.common.utils.LinkUtil;
import com.iib.platform.enach.models.ManageEnachModel;


/**
 * Sling Model Implementation of Manage Enach Model
 *
 * @author TechChefz (TCZ Consulting LLP)
 *
 */
@Model(adaptables=Resource.class, adapters=ManageEnachModel.class) 
public class ManageEnachModelImpl implements ManageEnachModel {
	
	private static Logger log = LoggerFactory.getLogger(ManageEnachModelImpl.class);

	@Inject @Optional
	@Default(values="")
	private String backButtonUrl;
	
	@Inject @Optional
	@Default(values="")
	private String buttonUrl;
	
	@PostConstruct
	public void init() {

	}

	@Override
	public String getBackButtonUrl() {
		return LinkUtil.getFormattedURL(backButtonUrl);
	}

	@Override
	public String getButtonUrl() {
		return LinkUtil.getFormattedURL(buttonUrl);
	}
	
}
