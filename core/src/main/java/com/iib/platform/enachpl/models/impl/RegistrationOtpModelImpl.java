package com.iib.platform.enachpl.models.impl;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.iib.platform.common.utils.LinkUtil;
import com.iib.platform.enachpl.models.RegistrationOtpModel;

@Model(adaptables=Resource.class, adapters=RegistrationOtpModel.class)
public class RegistrationOtpModelImpl implements RegistrationOtpModel {
	
	/*Logger*/
	private static Logger log = LoggerFactory.getLogger(RegistrationOtpModelImpl.class);
	
	@Inject @Optional
	@Default(values="")
	private String cancelUrl;
	
	
	@PostConstruct
	public void init() {
		
	}

	@Override
	public String getCancelUrl() {
		return LinkUtil.getFormattedURL(cancelUrl);
	}
}
