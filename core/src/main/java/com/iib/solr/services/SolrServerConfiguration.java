package com.iib.solr.services;

public interface SolrServerConfiguration {
	
	public String getSolrProtocol();

	public String getSolrServerName();

	public String getSolrServerPort();

	public String getSolrCoreName();

	public String getContentPagePath();

}
