package com.iib.solr.services.impl;

import java.net.MalformedURLException;

import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.iib.solr.services.SolrServerConfiguration;
import com.iib.solr.services.config.SolrServerConfig;

/**
 * Solr Server Configuration Service
 *
 * @author TechChefz (TCZ Consulting LLP)
 *
 */
@Component(
		service = { SolrServerConfiguration.class },
		name = "Solr Server Configuration",
		immediate = true,
		enabled = true,
		property = {
				Constants.SERVICE_DESCRIPTION + "=Solr Server Configuration Service"
		})
@Designate(ocd=SolrServerConfig.class)
public class SolrServerConfigurationImpl implements SolrServerConfiguration {
	
	private static final Logger LOG = LoggerFactory.getLogger(SolrServerConfigurationImpl.class);
	
	private String solrProtocol;
	private String solrServerName;
	private String solrServerPort;
	private String solrCoreName;
	private String contentPagePath;
	
	@Activate
	public void activate(SolrServerConfig config) throws MalformedURLException {
		LOG.info("SolrServerConfigurationImpl activated ");
		
		this.solrProtocol = config.protocolValue();
		this.solrServerName = config.serverName();
		this.solrServerPort = config.serverPort();
		this.solrCoreName = config.serverCollection();
		this.contentPagePath = config.serverPath();
	}
	
	@Override
	public String getSolrProtocol() {
		return solrProtocol;
	}

	@Override
	public String getSolrServerName() {
		return solrServerName;
	}

	@Override
	public String getSolrServerPort() {
		return solrServerPort;
	}

	@Override
	public String getSolrCoreName() {
		return solrCoreName;
	}

	@Override
	public String getContentPagePath() {
		return contentPagePath;
	}
	
}
