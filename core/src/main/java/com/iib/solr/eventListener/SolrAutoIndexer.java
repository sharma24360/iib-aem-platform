package com.iib.solr.eventListener;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.replication.ReplicationAction;
import com.day.cq.wcm.api.PageEvent;
import com.day.cq.wcm.api.PageManager;
import com.iib.solr.services.SolrSearchService;
import com.iib.solr.services.SolrServerConfiguration;

@Component(
		service = { EventHandler.class },
		name = "SolrAutoIndexer",
		immediate = true,
		enabled = true,
		property = {
				Constants.SERVICE_DESCRIPTION + "=" + "Solr Auto Indexer",
				EventConstants.EVENT_TOPIC + "=" + ReplicationAction.EVENT_TOPIC,
				EventConstants.EVENT_TOPIC + "=" + PageEvent.EVENT_TOPIC
		})
public class SolrAutoIndexer implements EventHandler {
	private static final Logger log = LoggerFactory.getLogger(SolrAutoIndexer.class);

	@Reference
	SolrServerConfiguration solrConfigurationService;

	@Reference
	SolrSearchService solrSearchService;

	@Reference
	private ResourceResolverFactory resourceResolverFactory;
	
	@Activate()
	public void activate() {
		log.info("SolrAutoIndexer activated!");
	}

	@Override
	public void handleEvent(Event event) {
		log.info("inside auto index");
		
		ResourceResolver resolver = null;
		final String protocol = solrConfigurationService.getSolrProtocol();
		final String serverName = solrConfigurationService.getSolrServerName();
		final String serverPort = solrConfigurationService.getSolrServerPort();
		final String coreName = solrConfigurationService.getSolrCoreName();
		String pagesResourcePath =  (String)((Object[])event.getProperty("paths"))[0];
		
		String URL = protocol + "://" + serverName + ":" + serverPort + "/solr/" + coreName;
		HttpSolrClient server = new HttpSolrClient(URL);
		Map<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put(ResourceResolverFactory.SUBSERVICE, "solrService");

		try {
			ReplicationAction action = ReplicationAction.fromEvent(event);
			if(action != null) {
				log.info("Replication action {} occured on {} ", action.getType().getName(), action.getPath());
			}
			
			if(action.getType().getName().equals("Activate")) {
				log.info("activated");
				resolver = resourceResolverFactory.getServiceResourceResolver(paramMap);
				log.info("Solr User ID:  " + resolver.getUserID());
				Resource pageResource = resolver.getResource(pagesResourcePath);
				pageResource = resolver.adaptTo(PageManager.class).getContainingPage(pageResource).getContentResource();
				JSONObject indexPageData = solrSearchService.createPageMetadataObject(pageResource);
				JSONArray array = new JSONArray();

				array.put(indexPageData);

				boolean resultindexingPages = solrSearchService.indexPagesToSolr(array, server);
				log.info(""+resultindexingPages);
			} 
			else if(action.getType().getName().equals("Deactivate")){
				log.info("Deactivate");
				resolver = resourceResolverFactory.getServiceResourceResolver(paramMap);
				Resource pageResource = resolver.getResource(pagesResourcePath);
				pageResource = resolver.adaptTo(PageManager.class).getContainingPage(pageResource).getContentResource();
				JSONObject indexPageData = solrSearchService.createPageMetadataObject(pageResource);
				boolean result = solrSearchService.deletePageFromSolr(indexPageData,server);
				log.info("Result :: " + result);
			}
			log.info("inside try of autoIndex");
		} catch (JSONException | SolrServerException e) {
			log.error("Exception due to", e);
		} catch (IOException e) {
			log.error("Exception due to", e);
			e.printStackTrace();
		} catch (LoginException e) {
			log.error( "Exception due to" +  e);
			e.printStackTrace();
		}
	}
}
